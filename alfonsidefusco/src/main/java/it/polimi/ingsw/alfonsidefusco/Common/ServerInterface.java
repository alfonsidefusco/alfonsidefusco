package it.polimi.ingsw.alfonsidefusco.Common;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ServerInterface extends Remote {
	
	void register(int port) throws RemoteException;

//	String joinMatch(String rules, String map) throws RemoteException;
}
