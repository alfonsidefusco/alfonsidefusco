package it.polimi.ingsw.alfonsidefusco.server.model.Actions;

import it.polimi.ingsw.alfonsidefusco.server.model.Match;
import it.polimi.ingsw.alfonsidefusco.server.model.Player;
import it.polimi.ingsw.alfonsidefusco.server.model.Position;


public class RedHatch implements Action {
	
	public void action(Match callingMatch, Player actor) throws Exception{
	
		Position escHatch= actor.getPosition();
		callingMatch.removeEscapeHatches(escHatch);
	}

}