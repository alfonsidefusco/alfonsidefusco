package it.polimi.ingsw.alfonsidefusco.server.model.Actions;
import java.util.Observable;
import java.util.Observer;

import it.polimi.ingsw.alfonsidefusco.server.model.*;


public class Adrenaline extends Move implements Action, Runnable, Observer {

	Player player;
	Position previousPosition;
	boolean updated = false;
	int mobility;
	
	public void action(Match callingMatch, Player actor ) {
		mobility = actor.getMobility();
		actor.setMobility( mobility+1 );
		player = actor;
		previousPosition = actor.getPosition();
	}
		
		
	public void update(Observable pl, Object obj) {
		if((Player) pl == player && !((Position) obj).equals(previousPosition) ) {
			player.setMobility( mobility );
			updated = true;
		}
	}


	public void run() {
		player.addObserver(this);
		while( !updated ) {
			try {
				Thread.sleep(3000);      //1000 milliseconds is one second.
			} catch(InterruptedException ex) {
			    Thread.currentThread().interrupt();
			}
		}	
		player.deleteObserver(this);
		return ;
	}

}