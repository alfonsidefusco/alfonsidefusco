package it.polimi.ingsw.alfonsidefusco.client.controller.cards;

import it.polimi.ingsw.alfonsidefusco.client.view.View;

public interface CardInterface {
	public void interact();
	public void setView(View screen);
}
