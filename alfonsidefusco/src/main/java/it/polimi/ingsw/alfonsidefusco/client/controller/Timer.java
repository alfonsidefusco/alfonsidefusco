package it.polimi.ingsw.alfonsidefusco.client.controller;

public class Timer implements Runnable {

	private final int TIMEOUT = 60; //60 seconds
	Postman instance;
	
	public void run() {
		try {
			Thread.sleep(TIMEOUT*1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if(!instance.answered)
			instance.timeout = true;
	}
	
	Timer(Postman instance) {
		this.instance = instance;
	}
}
