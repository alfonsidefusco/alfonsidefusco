package it.polimi.ingsw.alfonsidefusco.server.model.Cards;

import it.polimi.ingsw.alfonsidefusco.server.model.Match;
import it.polimi.ingsw.alfonsidefusco.server.model.Player;


public interface Card {
	
	boolean areFinished();
	
	void effect(Match match, Player player) throws Exception;
	
	void deduct();
	
	String toString();
	
	
}