package it.polimi.ingsw.alfonsidefusco.Common;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface ServerMatchInterface extends Remote {

	public List<PlayerClientInterface> downloadPlayersLog() throws RemoteException;
}
