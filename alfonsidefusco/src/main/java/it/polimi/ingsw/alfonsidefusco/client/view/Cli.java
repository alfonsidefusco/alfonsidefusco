package it.polimi.ingsw.alfonsidefusco.client.view;

import it.polimi.ingsw.alfonsidefusco.client.controller.Postman;
import it.polimi.ingsw.alfonsidefusco.client.controller.cards.Card;
import it.polimi.ingsw.alfonsidefusco.client.model.Map;
import it.polimi.ingsw.alfonsidefusco.client.model.MatchLog;
import it.polimi.ingsw.alfonsidefusco.client.model.PlayerClient;
import it.polimi.ingsw.alfonsidefusco.client.model.Rival;

import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;
import java.util.Set;

import org.apache.commons.io.FileUtils;

import static org.fusesource.jansi.Ansi.*;
import static org.fusesource.jansi.Ansi.Color.*;


public class Cli extends Map implements View, Observer {
	
	//FIELDS
	private char[] template;
	private SectorProperty[] secType;
	private int userPointer;
	private int[] othersPointer;
	private boolean legend = false;
	private Scanner in;
	
	
	//METHODS
			
	//return the template for the map specified in filePath
	private static char[] getTemplate(String filePath) throws IOException{
	
		File file;
		char[]	template = null;
		
		
		String workingDir = System.getProperty("user.dir");
		//Now build the path to the file of the map template
		filePath = workingDir + filePath;
		file = new File(filePath);
		template = FileUtils.readFileToString(file).toCharArray();
		return template;
	}
	

	//print label over char[] template in the pointer position
	private void overwrite(int pointer, char[] label) {
		for(int i = 0; i < label.length; i++) 
			template[pointer+i] = label[i];
	}
	
	
	private void fillMap() {
	
		char[] human = new char[] {'H', 'U', 'M'};
		char[] alien = new char[] {'A','L','N'};
		char[] missing = new char[] {' ',' ',' '};

		int position;
		char[] label = missing;
		for(Sector sec : super.prop) {	
				SectorProperty sp = sec.getType();
				position = toPointer((Position) sec);
				switch (sp) {
					case ESCAPE_HATCH:
						label = new char[] { '(', Character.forDigit( sec.getEscapeHatch() , 10 ), ')' };
						secType[position] = SectorProperty.ESCAPE_HATCH;
						break;
					case HUMAN_START:
						label = human;
						secType[position] = SectorProperty.HUMAN_START;
						break;
					case ALIEN_START:
						label = alien;
						secType[position] = SectorProperty.ALIEN_START;
						break;
					case DANGEROUS:
						secType[position] = SectorProperty.DANGEROUS;
						label = formatLabel((Position) sec);
						break;
					case SECURE:
						secType[position] = SectorProperty.SECURE;
						label = formatLabel((Position) sec);
						break;
					case NOT_EXISTS:
						break;
					default:
						label = formatLabel((Position) sec);
						break;
				}
				if( sp != SectorProperty.NOT_EXISTS) {
					overwrite(position, label);
				}
		}
	}
	
	
	private char[] formatLabel(Position pos) {
		
		char [] label = new char[3];
		label[0] = pos.getColumn();
		int num = pos.getRow();
		if(num<10) {
			label[1] = '0';
			label[2] = Character.forDigit(num, 10);
		}
		else
			label[1] = Character.forDigit(num/10, 10);
			label[2] = Character.forDigit(num%10, 10);
		return label;
	}

	
	//return how much characters to skip in BufferedReader in order to locate the given argument position
	private static int toPointer(Position position) {
	
		int horizontalShift;
		int verticalShift;
		
		//the horizontalShift depends on the character of the given position
		horizontalShift = Constant.SKIP_SLASH.value + Constant.COLUMN_OFFSET.value*position.getColumnInt() ;
		
		//Moreover since all odd characters (b,d,f,...) are down shifted of 1 line compared to even characters so
		if(position.getColumnInt()%2==0)
			verticalShift = Constant.ENTIRE_LINE.value;
		else
			verticalShift = 2*Constant.ENTIRE_LINE.value; 
		
		//the verticalShift depends on the number of the given position. "-1" is necessary 'cos numbers in the map start from 1
		verticalShift += ( (position.getRow() - 1) * Constant.ROW_OFFSET.value ) * Constant.ENTIRE_LINE.value;
				
		return ( horizontalShift + verticalShift ); 
	}
	
	
	private int[] othersPointer(Set<Position> players){
			
		int dim = players.size();
		int[] pointers = new int[dim];
		int i = 0;
		for(Position pos: players) {
			pointers[i] = toPointer(pos);
			i++;
		}
		return pointers;
	}
	
	
	public void showMap() {
		if(!legend) {
			System.out.print( ansi().fg(RED).a("RED").reset().a(" is your position").fg(YELLOW).a("\nYELLOW").reset().a(" are starting positions").fg(CYAN).a("\nCYAN").reset().a(" are dangerous sectors").reset().a("\nWHITE are secure sectors").fg(MAGENTA).a("\nMAGENTA").reset().a(" are your enemies").fg(GREEN).a("\nGREEN").reset().a(" your only way to survive...\n").reset() );
			legend = true;
		}
		userPointer = toPointer( super.user );
		othersPointer = othersPointer( super.others );
	
		SectorProperty type;
		boolean isColored = false;
		int limit = 3;
		for(int i = 0; i < template.length; i++) {
			if(secType[i] != null) {
				isColored = true;
				limit = 3;
				type = secType[i];
				switch(type) {
				case ESCAPE_HATCH:
					System.out.print( ansi().fg(GREEN) );
					break;
				case HUMAN_START:
					System.out.print( ansi().fg(YELLOW) );
					break;
				case ALIEN_START:
					System.out.print( ansi().fg(YELLOW) );
					break;
				case DANGEROUS:
					System.out.print( ansi().fg(CYAN) );
					break;
				default:
					break;
				}
			}
			if( i == userPointer ) {
				System.out.print( ansi().fg(RED) );
				isColored = true;
			}
			for(int n = 0; n< othersPointer.length; n++)
				if( i == othersPointer[n] ) {
					System.out.print( ansi().fg(MAGENTA) );
					isColored = true;
				}
			if( isColored ) {
				if( limit > 0 )
					limit--;
				else {
					System.out.print( ansi().reset() );
					isColored = false;
				}
			}
			System.out.print( template[i] );
		}
		System.out.println("");
	}
	
	public void update(Observable obs, Object arg) {
		if( obs instanceof PlayerClient ) {
			PlayerClient pl = (PlayerClient) obs;
			String info = (String) arg;
			
			if( info.equals( new String("mobility")) ) {
				showMsg("Currently you can move of " + pl.getMobility() + " cells" );
			}
			
			else if( info.equals( new String("position")) )
				super.user = new Position( pl.getPosition() );
			
			else if( info.equals( new String("setwin")) )
				showMsg("\n\nWELL DONE, YOU WIN!\n\n");
			
			else if( info.equals( new String("setwin")) )
				showMsg("\n\nYOU ARE DEAD\n\n");
		}
		
		else if( obs instanceof MatchLog ) {
			String info = (String) arg;
			MatchLog match = (MatchLog) obs;
			
			if( info.equals( new String("currentplayer") )) {			
				String current = new String("Current Player : " + match.getCurrentPlayer() );
				showMsg(current);
			}
			
			else if( info.equals( new String("me")) ) {
				clearConsole();
				showMap();
			}
				
			
			else if( info.equals( new String("round") )) {
				String round = new String("Round number : " + match.getRound() );
				showMsg(round);
			}
			
			else if( info.equals( new String("newplayer")) ) {
				showMsg("new player added");
			}
			
			else if( info.equals( new String("playerlog")) ) {
				try {
					rivals(match);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			else if( info.equals( new String("endgame")) ) {
				showMsg("\n\nGAME'S ENDED\n\n");
				showMsg("Quit and reopen to try another match.");
			}
		}
	}
	
	public void rivals(MatchLog match) throws RemoteException {
		System.out.println("=============================================================================================");
		//display names
		System.out.format("%-9.9s", "charac");
		for( Rival rival : match.getPlayersLog() )
			System.out.format("%.8s  ", rival.getName() );
		System.out.print("\n");
		//display last known positions
		System.out.format("%-9.9s", "known pos:");
		for( Rival rival : match.getPlayersLog() ) {
			if( rival.getPublicPosition() != null )
				System.out.format("%8s  ",  rival.getPublicPosition() );
			else
				System.out.format("%10s", "");
		}
		System.out.print("\n");
		
		if( match.getRules().equals( new String("complete")) ) {
		//display objectcards number
			System.out.format("%-9.9s", "obj cards");
			for( Rival rival : match.getPlayersLog() ) 
				if( rival.getObjectCardsNumber() != 0 )
					System.out.format("%8d  ", rival.getObjectCardsNumber());
				else
					System.out.format("%10s", "");
			System.out.print("\n");
		}
		//display nature
		System.out.format("%-9.9s", "nature");
		for( Rival rival : match.getPlayersLog() ) 
			System.out.format("%8s  ", rival.getNature());
		System.out.print("\n");
		//display winners
		System.out.format("%-9.9s", "state");
		String winner = new String("win");
		String dead = new String("dead");
		for( Rival rival : match.getPlayersLog() ) {
			if(rival.isWinner())
				System.out.format("%8s  ", winner);
			else if(rival.isDead())
				System.out.format("%8s  ", dead);
			else
				System.out.format("%10s", "");
		}
		System.out.print("\n");
		System.out.println("=============================================================================================");
	}
	
	@Override
	public void showMsg(String msg) {
		System.out.println(msg);
	}
	
	@Override
	public String waitAnswer() {
		return in.nextLine();
	}
		
	private final static void clearConsole()
	{
	    try
	    {
	        final String os = System.getProperty("os.name");

	        if (os.contains("Windows"))
	        {
	            Runtime.getRuntime().exec("cls");
	        }
	        else
	        {
	            Runtime.getRuntime().exec("clear");
	        }
	    }
	    catch (final Exception e)
	    {
	        //  Handle any exceptions.
	    }
	}	
	
	//CONSTRUCTORS
	public Cli(String mapName) {
			
		//fetch sector's description for specified map 
		super( mapName );
		
		//fetch map template
		String templatePath = "/resources/MapTemplates/"+mapName;
		try {
			template = Cli.getTemplate(templatePath);
		}
		catch (IOException e) {
			System.out.println("getTemplate: file specified as argument can't be found in default /resources/ folder");
		}
		secType = new SectorProperty[template.length];
		fillMap();
		
		Card cardInterface = new Card();
		cardInterface.setView(this);
		Postman.getInstance().setView(this);
		in = new Scanner(System.in);
	}
	

}
