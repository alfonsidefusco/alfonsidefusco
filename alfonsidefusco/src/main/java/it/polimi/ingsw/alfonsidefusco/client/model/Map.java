package it.polimi.ingsw.alfonsidefusco.client.model;

import it.polimi.ingsw.alfonsidefusco.client.view.Position;
import it.polimi.ingsw.alfonsidefusco.client.view.Sector;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;



public class Map {
	
	//MAP'S FILE VARIABLES
	private static Map instance;
	protected Position user = new Position("a1");	//This can be autoupdated any time the local player begin his turn.
	protected Set<Sector> prop = new HashSet<Sector>(336, (float) 0.05);
	protected Set<Position> others = new HashSet<Position>(7); //This field has to be udated through the Obserever pattern!!
	protected Set<Position> closedEscapeHatch;	//This field has to be udated through the Obserever pattern!!

	//METHODS
		
	//return a new reader from the fileName specified as argument. File must be in projectfolder or in a subfolder of it specified in within filePath.
	private BufferedReader getReader(String filePath) throws IOException, FileNotFoundException {
	
		File file;
		BufferedReader	reader = null;
			
		String workingDir = System.getProperty("user.dir");
		//Now build the path to the file of the property file
		filePath = workingDir + filePath;
		file = new File(filePath);
		reader = new BufferedReader(new FileReader(file));
		reader.readLine();
		return reader; 
	}

	//return the sector compiled with its properties read from a line of text given as argument
	private static Position fetchSector(String line) throws IndexOutOfBoundsException {
		
		Sector sec;
		String position = null;
		char prop = 0;
		
		
		//fetch property character of the sector
		int i = line.length()-1;
		while( !Character.isLetterOrDigit( line.charAt(i) ) )
			i--;
		prop = line.charAt(i);
		
		//get coordinates position relative to the property
		position = line.substring(0, i);
		
		
		//generate Sector
		if( Character.isDigit(prop) ) {
			int escapeHatch = Character.getNumericValue(prop);
			sec = new Sector(position, escapeHatch);
		}
		else {
			prop = Character.toUpperCase(prop);
			switch(prop) {
			case 'D':
				sec = new Sector(position, true, false, false);
				break;
			case 'S':
				sec = new Sector(position, false, false, false);
				break;
			case 'A':
				sec = new Sector(position, false, true, false);
				break;
			case 'H':
				sec = new Sector(position, false, false, true);
				break;
			case 'N':
				sec = new Sector(position, false);
				break;
			default :
				sec = new Sector(position, false);
				break;
			}
		}
		return sec;
	}

	//fill HashSet<Sector> class argument with secotrs of the specified map.
	private void generateMap(String path) throws IOException, FileNotFoundException {
		
		BufferedReader propertiesReader = getReader(path);
		String line = propertiesReader.readLine();
		do {
			try {
				prop.add( (Sector) fetchSector(line) );
			}
			catch (IndexOutOfBoundsException e) {
				System.out.println("Class Map > Map constructor > fetchSector : one line of property file skipped due to wrong definition");
			}
			line = propertiesReader.readLine();
		}			
		while( line != null);
	}

	protected static Map instance() {
		if(instance != null )
			return instance;
		else {
			System.out.println("THIS SHOULD NOT HAPPEN !!!!!!!!!! Map.instance() invoked before Map was initialized");
					return null;
		}
	}
	
	
	//CONSTRUCTOR
	protected Map(String fileName) {
	
		//fetch sector's properties of the map fileName
		String propertyPath = "/resources/MapDefinitions/"+fileName;
		try {
			generateMap(propertyPath);
		}
		catch (FileNotFoundException e) {
			System.out.println("File specified as argument can't be found in default /resources/ folder");
		}
		catch (IOException e) {
			System.out.println("getReader("+fileName+") found file but failed while reading it.");
		}
		instance = this;
		
	}
	
//	
//	//OBSERVERS
//	@Override
//	public void update(Observable o, Object arg) {
//	// TODO Auto-generated method stub
//	
//	}
	
	public void rivals(MatchLog match) throws RemoteException {
		showMsg("=============================================================================================");
		//display names
		showMsg(String.format("%-9.9s", "charac"));
		for( Rival rival : match.getPlayersLog() )
			showMsg(String.format("%.8s  ", rival.getName()));
		showMsg("\n");
		//display last known positions
		showMsg(String.format("%-9.9s", "known pos:"));
		for( Rival rival : match.getPlayersLog() ) {
			if( rival.getPublicPosition() != null )
				showMsg(String.format("%8s  ",  rival.getPublicPosition()));
			else
				showMsg(String.format("%10s", ""));
		}
		showMsg("\n");
		
		if( match.getRules().equals( new String("complete")) ) {
		//display objectcards number
			showMsg(String.format("%-9.9s", "obj cards"));
			for( Rival rival : match.getPlayersLog() ) 
				if( rival.getObjectCardsNumber() != 0 )
					showMsg(String.format("%8d  ", rival.getObjectCardsNumber()));
				else
					showMsg(String.format("%10s", ""));
			showMsg("\n");
		}
		//display nature
		showMsg(String.format("%-9.9s", "nature"));
		for( Rival rival : match.getPlayersLog() ) 
			showMsg(String.format("%8s  ", rival.getNature()));
		showMsg("\n");
		//display winners
		showMsg(String.format("%-9.9s", "state"));
		String winner = new String("win");
		String dead = new String("dead");
		for( Rival rival : match.getPlayersLog() ) {
			if(rival.isWinner())
				showMsg(String.format("%8s  ", winner));
			else if(rival.isDead())
				showMsg(String.format("%8s  ", dead));
			else
				showMsg(String.format("%10s", ""));
		}
		showMsg("\n");
		showMsg("=============================================================================================");
	}

	private void showMsg(String string) {
		// TODO Auto-generated method stub
		
	}

	
}



class SectorNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

	public SectorNotFoundException() {
		super();
	}
}
