package it.polimi.ingsw.alfonsidefusco.server.model.Cards.ObjectCards;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.alfonsidefusco.server.model.Cards.Card;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.Deck;

public class ObjectCards extends Deck {

	//FIELDS
	List<Card> deck = new ArrayList<Card>();

	@Override
	protected List<Card> getDeck() {
		return deck;
	}

	@Override
	protected void shuffle() {
		deck.clear();
		deck.add(new AdrenalineCard());
		deck.add(new AttackCard());
		deck.add(new DefenseCard());
		deck.add(new SedativeCard());
		deck.add(new SpotLightCard());
		deck.add(new TeleportCard());
	}

}
