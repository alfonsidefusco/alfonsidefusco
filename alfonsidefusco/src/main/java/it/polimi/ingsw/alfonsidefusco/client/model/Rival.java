package it.polimi.ingsw.alfonsidefusco.client.model;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Rival extends Remote {
	
	public String getName() throws RemoteException;
	
	public String getPublicPosition() throws RemoteException;
	
	public String getNature() throws RemoteException;
	
	public int getObjectCardsNumber() throws RemoteException;
	
	public boolean isDead() throws RemoteException;
	
	public boolean isWinner() throws RemoteException;
}
