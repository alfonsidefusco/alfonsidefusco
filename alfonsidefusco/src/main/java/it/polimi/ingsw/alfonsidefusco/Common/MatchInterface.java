package it.polimi.ingsw.alfonsidefusco.Common;

import java.rmi.Remote;
import java.rmi.RemoteException;


public interface MatchInterface extends Remote {
	
	public void increaseRound() throws RemoteException;

	public void setCurrentPlayer(String playerName) throws RemoteException;
	
	public String getMap() throws RemoteException;
	
	public String getRules() throws RemoteException;
	
	public void update(ServerMatchInterface matchSkeleton) throws RemoteException;
	
	public void endGame() throws RemoteException;
}