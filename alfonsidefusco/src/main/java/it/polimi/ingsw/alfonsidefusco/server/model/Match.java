package it.polimi.ingsw.alfonsidefusco.server.model;

import it.polimi.ingsw.alfonsidefusco.Common.MatchInterface;
import it.polimi.ingsw.alfonsidefusco.Common.PlayerClientInterface;
import it.polimi.ingsw.alfonsidefusco.Common.ServerMatchInterface;
import it.polimi.ingsw.alfonsidefusco.server.Exceptions.SectorNotFoundException;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.Deck;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.CharacterCards.CharacterCards;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.EscapeHatchCards.EscapeHatchCards;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.ObjectCards.ObjectCards;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.SectorCards.SectorCards;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Match implements ServerMatchInterface {

	//FILEDS
	public final int MAX_PLAYERS = 8;
	final Map playground;
	List<Player> players = new ArrayList<Player>(MAX_PLAYERS);
	private List<Position> escapeHatches = new ArrayList<Position>();
	private int round = 1;
	private Deck	sectorCard	= new SectorCards();
	private Deck	objectCards	= new ObjectCards();
	private Deck	hatchDeck	= new EscapeHatchCards();
	private CharacterCards characterCards;
	private Human	lastHuman;
	public List<MatchInterface> matchClient = new ArrayList<MatchInterface>();
	private List<PlayerClientInterface> playersLog = new ArrayList<PlayerClientInterface>();
	public ServerMatchInterface serverMatch;

	
	public void setStartPosition() {
	
		Sector humanStart = null, alienStart = null;
		for( Sector sec : playground.getSectors() ) {
			if( humanStart == null && sec.getType() == SectorProperty.HUMAN_START )
				humanStart = sec;
			if( alienStart == null && sec.getType() == SectorProperty.ALIEN_START )
				alienStart = sec;
		}
		for(Player pl : players) 
			if(pl instanceof Human)
				pl.setPosition(humanStart);
			else
				pl.setPosition(alienStart);
		
}
	
	//metodo di debug in sostituzione a quello sopra
	public void setStartPosition2() {
	
		Sector start = null;
		try {
		start = playground.findSector( new Position("H14") );
		} catch (SectorNotFoundException e) {
		e.printStackTrace();
		}
		for(Player pl : players) 
			pl.setPosition(start);

	}


	public boolean addPlayer(Player pl) {
		boolean success = false;
		if( players.size() <= MAX_PLAYERS ) {
			players.add(pl);
			success = true;
			playersLog.add( pl.getClient() );
		}
		return success;
	}
	

	public boolean isFull() {
		boolean full = true;
		if( players.size() < MAX_PLAYERS )
			full = false;
		else 
			full = true;
		return full; 
	}

	
	public List<Player> getPlayers() {
		List<Player> alive = new ArrayList<Player>( players.size() );
		for(Player pl : players)
			if( !( pl.isDead() || pl.isWinner() ) )
				alive.add(pl);
		return alive;
	}
	
	
	public List<Player> getAllPlayers() {
		return players;
	}
	
	
	public Human getLastHuman() {
		if( lastHuman == null )
			setLastHuman();
		return lastHuman;
	}


	public void setLastHuman() {
		List<Human> humans = new ArrayList<Human>( getPlayers().size() );
		for(Player pl : getPlayers()) 
			if(pl instanceof Human)
				humans.add((Human) pl);
		if(humans.size() == 1)
			lastHuman =  humans.get(0);
	}


	public Deck getHatchDeck() {
	return hatchDeck;
	}


	public Deck getObjectCards() {
		return objectCards;
	}


	public Deck getSectorCard() {
		return sectorCard;
	}


	public Map getMap() {
		return playground;
	}
	
	
	public void setAliensWinners() {
		for(Player pl : players)
			if(pl != null && pl instanceof Alien ) {
				pl.setWinner();
				try {
					pl.getClient().setWin(true);
				} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				}
			}
	//	setChanged();
	}
	
	
	public List<Player> getWinners() {
		List<Player> winners = new ArrayList<Player>( players.size() );
		for(Player pl : players)
			if(pl != null && pl.isWinner())
				winners.add(pl);
		return winners;
	}
	 
	
	@Deprecated
	public void setDead(Player pl) {
		pl.setDead();
	}
	
	private List<Position> findEscapeHatches(){
		for(Sector sec : playground.getSectors())
			if(sec.getType()==SectorProperty.ESCAPE_HATCH)
				escapeHatches.add(sec);
		
		return escapeHatches;
	}

	public List<Position> getEscapeHatches(){
		escapeHatches.removeAll(Collections.singleton(null));
		return escapeHatches;
	}

	public List<Position> removeEscapeHatches(Position ToRemoveHatch){
		for(int i = 0; i < escapeHatches.size(); i++) {
			Position cor= escapeHatches.get(i);
			   if(cor.equals(ToRemoveHatch))
			    escapeHatches.remove(cor);
		}
		for(Player pl : players)
			try {
				pl.getPostman().send("Escape Hatch in sector " + ToRemoveHatch.toString() + " can't be used no more." );
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		escapeHatches.removeAll(Collections.singleton(null));
		return escapeHatches;
	}

	public CharacterCards getCharacterCards() {
		return characterCards;
	}
	
	public void increaseRound() {
		round++;
		try {
			for(MatchInterface client : matchClient)
				client.increaseRound();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public int getRound() {
		return round;
	}
	
	public void setCurrentPlayer(Player player) {
		String playerName = player.getCharacter();
		try {
			for(MatchInterface client : matchClient)
				client.setCurrentPlayer( playerName );
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void addMatchClient(MatchInterface matchClient) {
		this.matchClient.add( matchClient );
	}
	
	
	public List<PlayerClientInterface> downloadPlayersLog() throws RemoteException {
		return playersLog;
	}
	
	
	public void endGame() {
		for(MatchInterface match : matchClient)
			try {
				match.endGame();
			} catch (RemoteException e) {
				e.printStackTrace();
			}
	}

	
	public boolean areHumans() {
		boolean areHumans = false;
		for(Player pl : players)
			if(pl instanceof Human) {
				areHumans = true;
				return areHumans;
			}
		return areHumans;
	}
	//CONSTRUCTOR
	public Match( String mapName ){
		playground = new Map(mapName);
		escapeHatches = findEscapeHatches();
		characterCards = new CharacterCards(1);
		try {
			serverMatch = (ServerMatchInterface) UnicastRemoteObject.exportObject((Remote) this, 0);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}