package it.polimi.ingsw.alfonsidefusco.client.view;

//import PolygonButton;
import it.polimi.ingsw.alfonsidefusco.client.controller.Postman;
import it.polimi.ingsw.alfonsidefusco.client.model.Map;
import it.polimi.ingsw.alfonsidefusco.client.model.MatchLog;
import it.polimi.ingsw.alfonsidefusco.client.model.PlayerClient;


import java.io.*;

import javax.swing.*;

import java.awt.event.*;
import java.awt.*;
import java.rmi.RemoteException;
import java.util.Observable;
import java.util.Observer;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;

import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

public class Gui extends Map implements View, Observer{
	
	final static int VERTICALBORDER= 7;
	final static int NCOLUMNS=23;
	final static int NROWS=14;
	final static int SECURE=1;
	final static int DANGEROUS=2;
	final static int ESCAPE_HATCH=3;
	final static int HUMAN_START=4;
	final static int ALIEN_START=5;
	final static int CLICKED=6;
	final static int MOVED=7;
	final static Color BGCOLOR= Color.BLACK;
	final static Color DANGEROUSCOLOR= Color.GRAY;
	final static Color SECURECOLOR= Color.WHITE;
	final static Color LINECOLOR= Color.BLACK;
	final static Color TEXTCOLOR=Color.DARK_GRAY;
	final static Color CLICKEDTEXTCOLOR= Color.RED;
	final static Color CLICKEDCOLOR= Color.YELLOW;
	final static Color HATCHCOLOR= Color.BLACK;
	int larg=0;
	int alt=0;
	int mapWidth=0;
	int mapHeight=0;
	String userDir = System.getProperty("user.dir");
	String humanStartLocation = userDir+"\\resources\\sectorImage\\humanStart.png";
	String alienStartLocation = userDir+"\\resources\\sectorImage\\alienStart.png";
	String humanStartLocation2 = userDir+"\\resources\\sectorImage\\humanStart2.png";
	String alienStartLocation2 = userDir+"\\resources\\sectorImage\\alienStart2.png";
	File humanStart = FileUtils.getFile(humanStartLocation);
	File alienStart = FileUtils.getFile(alienStartLocation);
	File humanStart2 = FileUtils.getFile(humanStartLocation2);
	File alienStart2 = FileUtils.getFile(alienStartLocation2);
	private JPanel panel;
	static int map[][]= new int[NROWS][NCOLUMNS];
	static int map1[][]= new int[NROWS][NCOLUMNS];
	String message= new String();
	int xFrame;
	int yFrame;
	String input= new String();
	
	private JTextArea notifications;
	private JTextField text;
	private boolean invoked=true;
	
	public Gui(String fileName) {
			super(fileName);
			init();
			createAndShowGUI();	
			Postman.getInstance().setView(this);
		}
	
	
	private void init(){
	// Sets the Jframe and adds the hexagonal map to it
		//Gui3 mappa= new Gui3("galilei");
		
		
		int x;
		int y;
		
		for(Sector sec: prop){
			x=sec.getRow();
			y=sec.getColumnInt();
			
			switch(sec.getType()){
			
			case SECURE : map[x-1][y]=SECURE;
				break;
			case DANGEROUS: map[x-1][y]=DANGEROUS;
				break;				
			case ESCAPE_HATCH : map[x-1][y]=ESCAPE_HATCH;
				break;
			case HUMAN_START: map[x-1][y]=HUMAN_START;
				break;	
			case ALIEN_START: map[x-1][y]=ALIEN_START;
				break;
			default:
				break;
			}
			map1[x-1][y]=map[x-1][y];
			
		}	    
	}
	
	private void createAndShowGUI(){
		
		JFrame frame = new JFrame();
		JPanel panel2= new JPanel();
		panel= new MappaEsagoni3();
	   
		Toolkit tk = Toolkit.getDefaultToolkit();
		
		setFrameSize(frame,tk);
		setSidePanel(panel2);
		setMapPanel(panel);
		
		addNotif(panel2);
	    addTextField(panel2);
		
	    frame.getContentPane().setLayout(new BorderLayout());
	    frame.getContentPane().add(panel2, BorderLayout.EAST);
	    frame.getContentPane().add(panel, BorderLayout.CENTER);
	    
	    frameDecorate(frame, tk);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.setLocationRelativeTo(null);
	    frame.setVisible(true);
	    frame.toFront();
		
		
	}
	
	private void setFrameSize(JFrame frame, Toolkit tk){
	    int xSize = ((int) tk.getScreenSize().getWidth());
	    int ySize = ((int) tk.getScreenSize().getHeight());	 
	    xFrame=(int)(xSize*0.9);
	    yFrame= (int)(ySize*0.8);
	    Dimension d=new Dimension(xFrame, yFrame);
	    frame.setSize(d);
		}
	
	private void frameDecorate(JFrame deco, Toolkit tk) {
		
		deco.setTitle("Escape from Aliens in Outer Space");
		Image titleIcon = tk.getImage(userDir+"\\resources\\sectorImage\\logo2.jpg");
		deco.setIconImage(titleIcon);
	}
	
	private void setMapPanel(JPanel adding) {
		
		adding.setBackground(Color.BLUE);
		MouseListener m= new MyMouseListener();
		adding.addMouseListener(m);
	}
	
	private void setSidePanel(JPanel panel){
		
		panel.setLayout(new BorderLayout());
		Dimension p= new Dimension((int)(xFrame*0.4),yFrame-20);	    
	    panel.setPreferredSize(p);
	}
	
	private void addNotif(JPanel adding) {
		
		notifications = new JTextArea();
		notifications.setLineWrap(true);
		notifications.setEditable(false);
		notifications.setVisible(true);
		JScrollPane scroll = new JScrollPane(notifications);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		adding.add(scroll, BorderLayout.CENTER);
	}
	
	private void addTextField(JPanel adding) {
	text = new JTextField();
	KeyListener key= new MyKeyListener();
	text.addKeyListener(key);
	Dimension dim= new Dimension((int)(xFrame*0.3),40);
	text.setPreferredSize(dim);
	adding.add(text, BorderLayout.SOUTH);
	}
	
	public class MyKeyListener implements KeyListener{

		@Override
		public void keyPressed(KeyEvent e) {
			int key=e.getKeyCode();
			
			input=input+""+e.getKeyChar();
			System.out.println(input);
			
			if(key==10){
				notifications.append("you chose "+input+"\n");
				//input="";
				text.setText("");
				invoked=false;
			}
			
			
		}

		@Override
		public void keyReleased(KeyEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void keyTyped(KeyEvent e) {
			
		}
		
	}

	protected Point pointInHex(int mx, int my, int larg, int alt){
		//mx and my are the coordinates of the mouse pointer
		Point notin = new Point(-1,-1);// this point is returned if the point clicked is not a valid hexagon.
		Point p= new Point();
		
		my-=VERTICALBORDER;
	
		int oddWidth=(NCOLUMNS/2+(NCOLUMNS&1))*larg;
		int evenWidth= ((NCOLUMNS-oddWidth/larg)/2+ (23&1))*larg;
		mapWidth= oddWidth+evenWidth-20;
		mapHeight= (NROWS+1)*alt-4;
		
		int x= mx/(3*larg/4);
		int y=(my-(x&1)*alt/2)/alt;
		
		int xins= mx-x*3*larg/4;
		int yins= my-y*alt;
		
		if(my-(x%2)*alt/2<0){
			System.out.println("xins-alt/2<0");
			return notin;
		}
		//even columns
		if((x&1)==0){
			
			if(yins<alt/2)// top half of hexagons
				if(xins*2<alt/2-yins){
					x--;
					y--;
				}
			
			if(yins>alt/2)// bottom half of hexagons
				if(xins*2<yins-alt/2 )
					x--;
			
		}
		else{ // odd columns
			
			if(yins<alt) // top half of hexagons
				if(xins*2<alt-yins)
					x--;
			
			if(yins>alt)// bottom half of hexagons
				if(xins*2<yins-alt){
					x--;
					y++;
				}
			}
		p.x=x;
		p.y=y;
		return p;
		
	}
	
	
	
	class MappaEsagoni3 extends JPanel{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
	
	
		public void paintComponent(Graphics g){
			super.paintComponent(g);
			Graphics2D d= (Graphics2D) g;
			d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			
			Dimension dim= getSize();	//frame size
			int panelWidth=dim.width;
			int panelHeight=dim.height;
			//this.setSize(panelWidth,panelHeight);	
			larg= panelWidth/18;//-(dim.width*30)/(100*18); // Hexagon's width	
			alt= panelHeight/15;
	//		JButton button= new JButton("oi");
	//		button.setBounds(dim.width-80, dim.height-30, 80, 30);
	//		this.add(button);
			
	
			for(int i=0; i<NROWS; i++)
				for(int j=0; j<NCOLUMNS; j++)
					drawHex(d, i, j, alienStart, humanStart);
			
		
		}
		
		
		private void drawHex(Graphics2D d, int i, int j, File alienStart, File humanStart){
	
				int Voffset=(alt/2)*(j&1); //  distance from the top of the frame of odd columns
				int Hoffset=j*3*larg/4;// distance from left side of the frame of every column
				int VBorder=7; 	//Distance from the top of the frame
				
				
				Rectangle2D.Double rec= new Rectangle2D.Double(Hoffset,i*alt+Voffset+VBorder,larg,alt);
						
				int yPoints[]={Voffset+(i*alt+(alt+VBorder)), Voffset+(i*alt+(alt+VBorder)), Voffset+(i*alt+(alt/2+VBorder)), Voffset+(i*alt+VBorder), Voffset+(i*alt+VBorder), Voffset+(i*alt+(alt/2+VBorder))};
				int xPoints[]={Hoffset+(3*larg/4)+1, Hoffset+larg/4+1, Hoffset-1, Hoffset+(larg/4)+1, Hoffset+(3*larg/4)+1, Hoffset+(larg)+1};
				Polygon poly= new Polygon(xPoints, yPoints, 6);
				
				Font strdfont= new Font("MicrosoftSansSerif", Font.PLAIN, (int)(12*(alt+larg)/78));
				Font font1= new Font("italic", Font.BOLD, (int)(13*(alt+larg)/78));
				Font font2= new Font("MicrosoftSansSerif", Font.BOLD, (int)(16*(alt+larg)/78));
				Font f;
				Color textcolor;
				Color linecolor;
				File AStart;
				File HStart;
				int xpoint;
				int ypoint=yPoints[2];
				
				if(i==user.getRow()-1 && j==user.getColumnInt()){
					textcolor= Color.ORANGE;
					f= font2;
					AStart= alienStart2;
					HStart= humanStart2;
					xpoint= xPoints[1]-4;
					linecolor= Color.RED;
					
					if(map[i][j]==ESCAPE_HATCH){
						xpoint= xPoints[1]-2;
						linecolor=Color.YELLOW;
					
						
					}
					}
				
				else{
					textcolor=TEXTCOLOR;
					linecolor=LINECOLOR;
					f= strdfont;
					AStart= alienStart;
					HStart= humanStart;
					xpoint= xPoints[1];
					
					if(map1[i][j]==ESCAPE_HATCH){
						f=font1;
						textcolor=Color.WHITE;
					}
					
				}
			
				if(map[i][j]==DANGEROUS)
					fillSectors(d, poly, i, j, xpoint, ypoint+5,DANGEROUSCOLOR, linecolor, textcolor, f);
				
				if(map[i][j]==SECURE)
					fillSectors(d, poly, i, j, xpoint, ypoint+5, SECURECOLOR, linecolor, textcolor, f);
				
				
				if(map[i][j]==ALIEN_START)
						fillPolygonWithImage(d, AStart, poly, rec);
				
	
				if(map[i][j]==HUMAN_START){
						fillPolygonWithImage(d, HStart, poly, rec);
				}
				
				if(map[i][j]==ESCAPE_HATCH){
					fillSectors(d, poly, i, j, xpoint-1, ypoint+5, HATCHCOLOR, linecolor, textcolor, f);
				}
				
				if(map[i][j]==CLICKED){
					
					if(map1[i][j]==ESCAPE_HATCH)		
						fillSectors(d, poly, i, j, xpoint, ypoint+5, Color.CYAN, linecolor, CLICKEDTEXTCOLOR, f);	
					
					else
						fillSectors(d, poly, i, j, xpoint, ypoint+5, CLICKEDCOLOR, linecolor, CLICKEDTEXTCOLOR, f);				
				}			
			}
			
		private void fillSectors(Graphics2D d, Polygon poly, int i, int j, int xtext, int ytext, Color insidecolor, Color linecolor, Color textcolor, Font font){
			
			String coordinates= new String();	
			d.setColor(insidecolor);
			d.fillPolygon(poly);
			d.setColor(linecolor);
			d.drawPolygon(poly);
			Position pos= new Position(j,i);
			char letter = pos.getColumn();
			d.setColor(textcolor);
			d.setFont(font);
			if((i+1)<10)
				coordinates=""+letter+"0"+(i+1);
			else
				coordinates=""+letter+(i+1);
			d.drawString(coordinates, xtext, ytext);
			
		}
	
		private void fillPolygonWithImage(Graphics2D d, File name, Polygon p, Rectangle2D rec){
			BufferedImage bi=null;
			try {
				bi = ImageIO.read(name);
			} catch (IOException e) {
				e.printStackTrace();
			}
			TexturePaint paint= new TexturePaint(bi, rec);
			d.setPaint(paint);
			d.fillPolygon(p);
		}	
	}
	
	class MyMouseListener implements MouseListener{
		int oldx=0;
		int oldy=0;
		@Override
		public void mouseClicked(MouseEvent e) {
			int x= e.getX();
			int y= e.getY();
			Point p= pointInHex(x, y, larg, alt);
			
			Position pos= new Position(p.x,p.y+1);		
		
				if(x<0|| y<0 || x>mapWidth || y>mapHeight)
					return;
			
			
				if(p.x!=-1 && p.y!=-1){	
					int curr= map[p.y][p.x];
					
					if(e.getClickCount()==2){
						if(map[p.y][p.x]==SECURE || map[p.y][p.x]==DANGEROUS || map[p.y][p.x]==ESCAPE_HATCH){
							if(user!=pos){	
								input=""+pos.getColumn()+pos.getRow()+"\n";
								invoked=false;
							}
						}
						panel.repaint();
					}
						//panel.repaint();
					else{
					if(curr==6)
						map[p.y][p.x]=map1[p.y][p.x];
					
					
					else{
						if(map[p.y][p.x]==SECURE || map[p.y][p.x]==DANGEROUS || map[p.y][p.x]==ESCAPE_HATCH){	
						map[oldy][oldx]=map1[oldy][oldx];
						map[p.y][p.x]=6;
						oldx=p.x;
						oldy=p.y;
					}
					}
						
					panel.repaint();
					}
				}
					
			
			
		}
	
		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
	
		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
	
		@Override
		public void mousePressed(MouseEvent e) {
			//mouseClicked(e);
			
		}
	
		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
	}

	@Override
	public void showMsg(String msg) {
		notifications.append(msg+"\n");
		
	}


	@Override
	public String waitAnswer() {
		invoked=true;
		while(invoked) {
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
		}
		//int i=0;
		
	
		input = input.substring(0, input.length()-1);
		
		return input;
	}


	@Override
	public void update(Observable obs, Object arg) {
		if( obs instanceof PlayerClient ) {
			PlayerClient pl = (PlayerClient) obs;
			String info = (String) arg;
			
			if( info.equals( new String("mobility")) ) {
				showMsg("Currently you can move of " + pl.getMobility() + " cells" );
			}
			
			else if( info.equals( new String("position")) ){
				super.user = new Position( pl.getPosition() );
				panel.repaint();
			}
			
			else if( info.equals( new String("setwin")) )
				showMsg("\nWELL DONE, YOU WIN!\n");
			
			else if( info.equals( new String("setwin")) )
				showMsg("\nYOU ARE DEAD\n");
		}
		
		else if( obs instanceof MatchLog ) {
			String info = (String) arg;
			MatchLog match = (MatchLog) obs;
			
			if( info.equals( new String("currentplayer") )) {			
				String current = new String("Current Player : " + match.getCurrentPlayer() );
				showMsg(current);
			}
			
			else if( info.equals( new String("me")) ) {
			
			}
				
			
			else if( info.equals( new String("round") )) {
				String round = new String("Round number : " + match.getRound() );
				showMsg(round);
			}
			
			else if( info.equals( new String("newplayer")) ) {
				showMsg("new player added");
			}
			
			else if( info.equals( new String("playerlog")) ) {
				try {
					rivals(match);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			else if( info.equals( new String("endgame")) ) {
				showMsg("\nGAME'S ENDED\n");
				showMsg("Quit and reopen to try another match.");
			}
		}
		}
	
	
		
	}
				




