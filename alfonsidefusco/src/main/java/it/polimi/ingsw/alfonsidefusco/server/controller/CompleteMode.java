package it.polimi.ingsw.alfonsidefusco.server.controller;

import it.polimi.ingsw.alfonsidefusco.server.Exceptions.*;
import it.polimi.ingsw.alfonsidefusco.server.model.Alien;
import it.polimi.ingsw.alfonsidefusco.server.model.Human;
import it.polimi.ingsw.alfonsidefusco.server.model.Player;
import it.polimi.ingsw.alfonsidefusco.server.model.Position;
import it.polimi.ingsw.alfonsidefusco.server.model.SectorProperty;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.Card;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.ObjectCards.DefenseCard;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.ObjectCards.SpotLightCard;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.SectorCards.NoiseWithObject;

import java.rmi.RemoteException;
import java.util.concurrent.CopyOnWriteArrayList;

public class CompleteMode extends BasicMode {

	//FIELDS
	final int HUMAN_MAX_DISTANCE = 1;
	final int ALIEN_MAX_DISTANCE = 2;
	final int ALIEN_FEEDING = 3;
	final static int MAX_ROUNDS = 39;
	//METHODS
	
	@Override
	public void humanTurn(Human pl) throws RemoteException {
		try {
			objectCardWrap(pl);
		} catch (RemoteException e2) {
			e2.printStackTrace();
		}
		SectorProperty dest = null;
		try {
			dest = moveWrap(pl);
		} catch (RemoteException e1) {
			e1.printStackTrace();
		}
		if(dest == SectorProperty.ESCAPE_HATCH )
			hatchWrap(pl);
		else if (dest == SectorProperty.DANGEROUS ) {
			if( !pl.hasSedative() ) {
				try {
					drawSectorCard(pl);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}
			else pl.setSedative(false);
		}
		try {
			objectCardWrap(pl);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
			
	}
	
	@Override
	public void alienTurn(Alien pl) throws RemoteException {
		super.alienTurn(pl);
	}
	
	@Override
	public void win() {
		//I case is handeld in GreenHatch Action by setting player as escaped
	
	
		// II case: last human is killed or all humans have been killed
		Human lastHuman =  match.getLastHuman();
		//if it isn't clear who is the last, lastHuman = null
		//it may happen if last two humans or more die at the same time
		
		int allHumans = 0;
		int killedHumans = 0;
		for( Player pl: match.getAllPlayers() )
			if( pl instanceof Human ) {
				allHumans++;
				if( pl.isDead() )
					killedHumans++;
			}
		
		boolean killedAll = killedHumans==allHumans? true : false;
		if( lastHuman == null && killedAll ) //aliens killed ALL humans
			match.setAliensWinners();
		else if ( lastHuman != null && lastHuman.isDead() ) //aliens didn't killed necessary all humans but killed the last one
			match.setAliensWinners();
	
		
		// III case
		else if( match.getRound() == MAX_ROUNDS ) {
			int humansAlive = allHumans-killedHumans;
			if( humansAlive <= 1 ) {
				match.setAliensWinners();
				endGame = true;
			}
		}
		
		//endgame condition
		if(killedAll)
			endGame = true;
				
	}
	
	
	private void hatchWrap(Human pl) {
		try {
		match.getHatchDeck().draw().effect(match, pl);
		} catch (Exception e) {
		e.printStackTrace();
		}
		if( match.getEscapeHatches().size() == 0 )
			for(Player pl1  : match.getPlayers() )
				if(pl1 instanceof Human)
					pl1.setDead();
	}

	
	@Override 
	Card drawSectorCard(Player pl) throws RemoteException {
		Card card = super.drawSectorCard(pl);
		if( card instanceof NoiseWithObject) {
			while(true) {
				try {
					pl.addObject( match.getObjectCards().draw() );
					break;
				} catch( ReachObjectCardsLimitException e ) {
					boolean correctInput = false;
					
					while( !correctInput ) {
						listObjectCards(pl);
						int toRemove = Integer.parseInt( pl.getPostman().ask("You have reached the limit of object cards. "
								+ "Type the number of the card you want to replace with that drawn") ) -1;
						if( toRemove>0 && toRemove<pl.OBJECT_CARDS ) {
							pl.removeObject( toRemove);
							correctInput = true;
						}
					}
					
					continue;
				}
			}
			
		sendToAll( pl.getCharacter() + " drew an object card."); 
		}
		return card;
	}	
	
	private void alienAttackFeed(Alien pl) {
	
		int mobility = pl.getMobility();
		int alive = match.getPlayers().size();
		
		super.attackWrap(pl);
		
		int stillAlive = match.getPlayers().size();
		if( stillAlive < alive )
			pl.setMobility( mobility + 1 );
				
	}

	@Override
	void attackWrap(Alien aln) {
		//Setting attacked match.getPlayers() with defense object to invincible
		for( Player player : match.getPlayers() )
			if(player.getPosition() == aln.getPosition() ) {  	
				CopyOnWriteArrayList<Card> objects = new CopyOnWriteArrayList<Card>( player.getObjects() );
				for( Card card : objects )	
					if( card instanceof DefenseCard ) {
						try {
							card.effect(match, player);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
			}
		
		//execution of attack);
		if(aln.getMobility() < ALIEN_FEEDING )
			alienAttackFeed(aln);
		else
			super.attackWrap(aln);

	}	
	
	private void objectCardWrap(Player pl) throws RemoteException {
		if( !pl.getObjects().isEmpty() ) {
			listObjectCards(pl);
			boolean correctInput= false;
			while(!correctInput) {
				String useObjects = pl.getPostman().ask("Do you want to use one of your object cards ? [yes/no]");
				if( useObjects.equalsIgnoreCase(new String("yes")) ) {
					correctInput=true;
					pl.getClient().logNature( pl.getNature() );
					boolean correctInput2 = false;
					String input = null;
					Card card = null;
					while(!correctInput2) {
					
						
						
						input = pl.getPostman().ask("Type the corrisponding number to use it or 0 to go through");
						int choice = Integer.parseInt( input ) -1;
						if(choice >= 0 && choice < 3) 
							try {
								card = pl.getObjects().get(choice);
								if( card instanceof SpotLightCard )
									SpotLight(card, pl);
								card.effect(match, pl);
								sendToAll(pl.getCharacter() + " uses " + card.toString() );
								pl.getClient().logNature( pl.getNature() );
								correctInput2 = true;
							} catch (IllegalSectorException e1) {
								pl.getPostman().send("Invalid sector, choose another one!");
								continue;
							} catch( SectorNotFoundException e2) {
								pl.getPostman().send("Sector not Found");
								continue;
							} catch (Exception e) {
								e.printStackTrace();
								break;
							} 
						else
							correctInput2 = true;
					}
					pl.getObjects().remove(card);
					pl.getClient().logDiscard();
				}
				else if( useObjects.equalsIgnoreCase( new String("no")) )
					correctInput=true;
				else ;
			}
		}
	}

	
	private void listObjectCards(Player pl) throws RemoteException {
		pl.getPostman().send("You have the following object cards:");
		int i = 1;
		for(Card crd : pl.getObjects() ) {
			pl.getPostman().send("("+ i +") " + crd.toString() );
			i++;
		}
	}

	
	private void SpotLight(Card card, Player pl) throws RemoteException {
		String position = null;
		position = pl.getPostman().ask("Spotlight Card: insert the target position for the card effect");
		SpotLightCard crd = (SpotLightCard) card;
		crd.setTargetPosition( new Position( position ));
	}
	
	
 
	CompleteMode() {
		super.rules = new String("complete");
	}
}
