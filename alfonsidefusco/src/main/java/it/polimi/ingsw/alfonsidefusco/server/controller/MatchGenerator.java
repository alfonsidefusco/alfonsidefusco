package it.polimi.ingsw.alfonsidefusco.server.controller;



import it.polimi.ingsw.alfonsidefusco.server.model.Match;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MatchGenerator {

	private static MatchGenerator instance;
	private List<MatchHandler> games = new ArrayList<MatchHandler>();
	
	
	MatchHandler findGame(String rules, String map) {

		MatchHandler matchFound = null;
		for( MatchHandler mh : games )
			if(mh.match.getMap().name().equals(map) && mh.rules.equals( rules ) ) 
				if( !mh.match.isFull() && !mh.alreadyStarted() )
					matchFound = mh;
		if(matchFound == null) {
			Match newMatch = new Match(map);
			MatchHandler mh;
			if( rules.equals("basic") )
				mh = new BasicMode();
			else
				mh = new CompleteMode();
			mh.match = newMatch;
			games.add(mh);
			matchFound = mh;
			System.out.println("New match in " + map + " with " + rules + " rules created");
		}
		return matchFound;
	}
	
	void removeMatch(Match toBeRemoved) {
		Iterator<MatchHandler> it = games.iterator();
		while( it.hasNext() )
			if(it.next().match == toBeRemoved)
				it.remove();
	}
	
	List<MatchHandler> getAllGames() {
		return games;
	}
	
	static MatchGenerator getInstance() {
		if( instance == null)
			instance = new MatchGenerator();
		return instance;
		
	}
}
