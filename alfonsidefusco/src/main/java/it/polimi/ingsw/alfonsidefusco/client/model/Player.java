package it.polimi.ingsw.alfonsidefusco.client.model;

import it.polimi.ingsw.alfonsidefusco.client.view.Position;
import it.polimi.ingsw.alfonsidefusco.client.view.Sector;
import it.polimi.ingsw.alfonsidefusco.client.view.SectorProperty;


public class Player {

	Position position;
	//ObjectCard
	//CharacterCard
	//id: String
	//available actions
	boolean human = true;
	
	
	private void setStartPosition() {
		Position start = null;
		SectorProperty faction;
		if( human )
			faction = SectorProperty.HUMAN_START;
		else
			faction = SectorProperty.ALIEN_START;
		for( Sector sec : Map.instance().prop)
			if( sec.getType() == faction )
				start = sec.getPosition();
		position = start;
	}

	
	public Player() {
		setStartPosition();
		Map.instance().user = position;
	}
	
}