package it.polimi.ingsw.alfonsidefusco.server.model.Actions;

import it.polimi.ingsw.alfonsidefusco.server.Exceptions.*;
import it.polimi.ingsw.alfonsidefusco.server.model.*;

import java.util.ArrayList;
import java.util.List;


public class Move implements Action{	
	Match callingMatch;
	Position desti;
	Player actor;
	public void setTargetPosition(Position pos) {
		desti = pos;
	}
	
	
	public void action(Match callingMatch, Player actor) throws SectorNotFoundException, OutOfBoundsException, StupidUserException, EscapeHatchClosedException {
		this.callingMatch=callingMatch;
		this.actor=actor;
		Sector destination=null;




			
			
			destination = callingMatch.getMap().findSector(desti);

			List<Position> possMoves = new ArrayList<Position>();
			possMoves = checkPossibleMoves();

			checkDestination(possMoves, destination);

			
		
		
	}
	
	//returns the list of positions that the player can reach starting from the given position having the given number of moves in the given map
	private List<Position> checkPossibleMoves() throws SectorNotFoundException{
		Sector start= actor.getPosition();		
	    List<Position> visited = new ArrayList<Position>();
		visited.add(start);
		// List of all the sectors reachable for the player
		List<Position> fringes= new ArrayList<Position>();
		fringes.add(start);
		int k;
		int dir;
		int count=0;
		Position s;
		Position neighbor;
		

		for(k=0; k<actor.getMobility(); k++){
			int size= fringes.size();
			
		    for(; count<size; count++){
		    	s= fringes.get(count);
		    	
		        for(dir=0; dir>=0 && dir<6; dir++){
		        	neighbor= neighbor(s, dir);
		        	
		        	if(!containsEqual(visited, neighbor)){
		        		visited.add(neighbor);
		        		
		        		if(!isBlocked(neighbor))
		        			fringes.add(neighbor);
		        	}
		        }
		    }
		}	
		   
		
	return fringes;
	
	}
	
	private void checkDestination(List<Position> possibleMoves, Sector destination) throws SectorNotFoundException, OutOfBoundsException, StupidUserException, EscapeHatchClosedException{
		
		Sector start=actor.getPosition();
		
		if(containsEqual(possibleMoves, destination)){
			actor.setPosition(destination);
			return;
			}
		
		if(((destination.getType()==SectorProperty.ESCAPE_HATCH) && !containsEqual(callingMatch.getEscapeHatches(), destination)))
			throw new EscapeHatchClosedException();
		
		if(start.getColumnInt()==destination.getColumnInt() && start.getRow()==destination.getRow()||isBlocked(destination))
			throw new StupidUserException();
	
		throw new OutOfBoundsException();
		
	}
	
	//checks if is there a position in the given list which is equal to the given position
	private boolean containsEqual(List <Position> container, Position toFind){
		int i;
    	boolean isContained =false;
    	
		for(i=0; i<container.size() && !isContained ;i++)
    		if(container.get(i).equals(toFind))
    			isContained=true;
		return isContained;
	}
	
	
	//returns true if the sector corresponding to the given position doesn't exists inside the map or it's an alien or human start
	private boolean isBlocked(Position pos){
		SectorProperty prop=null;
		boolean blocked= false;
		
			try {
				prop= callingMatch.getMap().checkSector(pos);
			} catch (SectorNotFoundException e) {}
		
		
		if(prop==SectorProperty.NOT_EXISTS || prop==SectorProperty.ALIEN_START || prop==SectorProperty.HUMAN_START || ((prop==SectorProperty.ESCAPE_HATCH) && !containsEqual(callingMatch.getEscapeHatches(), pos)))
			blocked=true;
		
		if(actor instanceof Alien && prop==SectorProperty.ESCAPE_HATCH)
			blocked=true;
		
		return blocked;	
	}
	
	
	// given a position and a number it returns the position adjoining the given one corresponding to the given number
	//The neighbor sectors are numbered starting from the one at north-west of the given position and going on clockwise around the given position 
	public Position neighbor(Position sector, int dir){
		
		Position neighbor= new Position();
		int col=sector.getColumnInt();
		int row=sector.getRow();
		
		try {
			if(dir==0){
				Position neighbor0= new Position(col-1, row -(1-(col&1)));
				return neighbor0;
			}
			
			if(dir==1){
				Position neighbor1= new Position(col, row -1);
				return neighbor1;
			}
			
			if(dir==2){
				Position neighbor2= new Position(col+1, row -(1-(col&1)));
				return neighbor2;
			}
			
			if(dir==3){
				Position neighbor3= new Position(col+1, row +(col&1));
				return neighbor3;
			}
			
			if(dir==4){
				Position neighbor4= new Position(col, row +1);
				return neighbor4;
			}
			
			if(dir==5){
				Position neighbor5= new Position(col-1, row +(col&1));
				return neighbor5;	
			}
		}
		catch( StringIndexOutOfBoundsException ex) {}
		return neighbor;
		
		
	}

	

//		public static void main (String[] args){
//			Match partita = new BasicMode();
//			Position s;
//			//Position t;
//			
//			Player player1;
//			Move a;
//			//Map mappa= new Map("galilei");
//			
//			player1=new Player();
//			
//			
//			Scanner prendi=new Scanner(System.in);
//			System.out.println("Movimento massimo: ");
//			int movement= prendi.nextInt();
//			player1.setMobility(movement);
//			boolean error=true;
//			while(error){
//				error=false;
//				System.out.println("Settore di partenza: ");
//				String partenza= new String(prendi.next());
//				//System.out.println("Settore di destinazione: ");
//				//String destinazione= new String(prendi.next());	
//						
//				s=new Position(partenza);
//				//t=new Position(destinazione);
//				Sector z=null;
//				try {
//					z = partita.getMap().findSector(s);
//				} catch (SectorNotFoundException e) {
//					error=true;
//					System.out.println("Sector not found!");
//					continue;
//					//e.printStackTrace();
//				}
//				
//				player1.setPosition(z);
//			}
//			System.out.println("Premere 1 per eliminare la seconda scialuppa 0 se non si vuole togliere niente");
//			int RemoveHatch= prendi.nextInt();
//			Position esc= new Position("v02");
//			
//			System.out.println(""+partita.getEscapeHatches().size());
//					
//			if(RemoveHatch==1){
//				
//				System.out.println(""+partita.getEscapeHatches().size());
//						partita.removeEscapeHatches(esc);
//				
//				System.out.println(""+partita.getEscapeHatches().size());
//				
//				}
//			a= new Move();
//		
//			int menu=1;
//			while(menu==1){
//			prendi.reset();
//				/* System.out.println("Settore di destinazione: ");
//			     prendi.reset();
//				String destinazione= new String(prendi.next());	
//				Position t=new Position(destinazione);
//				try {
//					Sector y= partita.getMap().findSector(t);
//				} catch (SectorNotFoundException e) {
//					System.out.println("Sector not found!");
//					//e.printStackTrace();
//				}*/
//				
//			a.action(partita, player1);
//			
//			
//			System.out.println("Press 1 to continue or 0 to exit");
//			menu= prendi.nextInt();
//		
//			//prendi.close();
//		}
//			prendi.close();
//		}	
	
	}


