package it.polimi.ingsw.alfonsidefusco.server.model.Actions;

import it.polimi.ingsw.alfonsidefusco.server.model.Human;
import it.polimi.ingsw.alfonsidefusco.server.model.Match;
import it.polimi.ingsw.alfonsidefusco.server.model.Player;



public class Sedatives implements Action{
	public void action(Match callingMatch, Player actor) throws Exception{	
		Human humanActor = (Human) actor;
		humanActor.setSedative(true);
	}


}