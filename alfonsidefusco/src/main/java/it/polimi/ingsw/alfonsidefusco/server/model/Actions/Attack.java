package it.polimi.ingsw.alfonsidefusco.server.model.Actions;

import java.rmi.RemoteException;
import java.util.ConcurrentModificationException;

import it.polimi.ingsw.alfonsidefusco.server.model.Match;
import it.polimi.ingsw.alfonsidefusco.server.model.Player;
import it.polimi.ingsw.alfonsidefusco.server.model.Sector;

public class Attack implements Action {
	public static String name = "Attack";
	
	public void action(Match match, Player player) throws ConcurrentModificationException {
		Sector target= player.getPosition();
			for( Player pl : match.getPlayers() )
				if( pl.getPosition().equals(target) && pl!=player )
					pl.setDead();
			
		try {
			player.getClient().log( player.getPosition().toString() , player.getNature() );
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}


}
