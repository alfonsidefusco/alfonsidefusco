package it.polimi.ingsw.alfonsidefusco.server.model.Actions;

import it.polimi.ingsw.alfonsidefusco.server.model.Match;
import it.polimi.ingsw.alfonsidefusco.server.model.Human;
import it.polimi.ingsw.alfonsidefusco.server.model.Player;


public class GreenHatch extends RedHatch implements Action{
	
	@Override
	public void action(Match callingMatch, Player actor) throws Exception{

		super.action(callingMatch, actor);
		Human human = (Human) actor;
		human.setEscaped();
		
	}
}