package it.polimi.ingsw.alfonsidefusco.server.model.Cards.ObjectCards;

import it.polimi.ingsw.alfonsidefusco.server.model.Match;
import it.polimi.ingsw.alfonsidefusco.server.model.Actions.Sedatives;
import it.polimi.ingsw.alfonsidefusco.server.model.Player;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.Card;

public class SedativeCard implements Card {
	private static int quantity = 3;
	public static final String name = new String("Sedative"); 
	
	@Override
	public boolean areFinished() {
		boolean finished = false;
		if (quantity == 0 )
			finished = true;
		return finished;	
	}
	
	@Override
	public void effect(Match match, Player player) throws Exception {
		new Sedatives().action(match, player);
	}
	
	@Override
	public void deduct() {
		quantity--;
	}

	public String toString() {
		return name;
	}
}
