package it.polimi.ingsw.alfonsidefusco.server.model.Cards.EscapeHatchCards;

import java.util.ArrayList;
import java.util.List;

import it.polimi.ingsw.alfonsidefusco.server.model.Cards.Card;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.Deck;

public class EscapeHatchCards extends Deck {

	//FIELDS
	List<Card> deck = new ArrayList<Card>();
	
	
	@Override
	protected List<Card> getDeck() {
		return deck;
	}
	
	@Override
	protected void shuffle() {
		deck.clear();
		deck.add(new Green());
		deck.add(new Red());
		
	}

}
