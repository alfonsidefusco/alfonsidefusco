 package it.polimi.ingsw.alfonsidefusco.server.model.Cards.SectorCards;

import it.polimi.ingsw.alfonsidefusco.server.model.Cards.Card;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.Deck;

import java.util.ArrayList;
import java.util.List;



public class SectorCards extends Deck {

	//FILEDS
	List<Card> deck = new ArrayList<Card>();

	//METHODS
	@Override
	protected void shuffle() {
		deck.clear();
		deck.add(new NoiseAllSector());
		deck.add(new NoiseAllSectorObj());
		deck.add(new NoiseYourSector() );
		deck.add(new NoiseYourSectorObj());
		deck.add(new Silence());
	}
	
	@Override
	protected List<Card> getDeck() {
		return deck;
	}
	
}
