package it.polimi.ingsw.alfonsidefusco.client.controller.cards;

public class Adrenaline extends Card implements CardInterface {
	
	@Override
	public void interact() {
		screen.showMsg("Adrenaline Card activated. Now you 'll be able to move of 2 sectors for this turn.");
	}

}
