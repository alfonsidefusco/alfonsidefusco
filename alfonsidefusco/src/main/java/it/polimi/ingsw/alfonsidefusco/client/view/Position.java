package it.polimi.ingsw.alfonsidefusco.client.view;
/**It'sused to describe a position of the ground map
 * 
 * @author Tommy
 *
 */
public class Position {
	private char ch;
	private int chNumber; //it express the numeric value corresponding to ch in alphabeti order starting count from 0
	private int num = 0;
	private final String abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
	public Position(String coord) {
		
		int i = 0;
		
		//skips whitespaces
		while( !Character.isLetter( coord.charAt(i) ) )
			i++;
		
		//get a single character
		ch = Character.toUpperCase( coord.charAt(i) );
		
		//skips whitespaces
		while( !Character.isDigit( coord.charAt(i) ) )
			i++;
		
		//get number(s)
		for(num=0 ; i < coord.length() && Character.isDigit(coord.charAt(i)); i++) {
			num = num*10 +  Character.getNumericValue( coord.charAt(i) );
		}
		
		for(chNumber=0; ch!=abc.charAt(chNumber); chNumber++);
	}
	
	public Position(int col, int row){
		this.chNumber=col;
		this.num=row;
		this.ch=abc.charAt(col);
		
		
	}
	
	public int getRow() {
		return num;
	}
	
	public int getColumnInt() {
		return chNumber;
	}
	
	public char getColumn() {
		return ch;
	}

	public Position getPosition() {
		return this;
	}
	
}

