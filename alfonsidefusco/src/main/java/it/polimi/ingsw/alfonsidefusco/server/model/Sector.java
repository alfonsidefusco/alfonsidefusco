package it.polimi.ingsw.alfonsidefusco.server.model;

public class Sector extends Position{
	
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	private boolean exists = true;
	private boolean dangerous = false;
	private boolean humanStart = false;
	private boolean alienStart = false;
	private int escapeHatch = 0;
	
	//CONSTRUCTOR
	public Sector(String position, int escapeHatch) {
		super(position);
		dangerous = false;
		humanStart = false;
		alienStart = false;
		this.escapeHatch = escapeHatch;
	}
	
	public Sector(String position, boolean exists) {
		super(position);
		this.exists = exists;
	}
	
	public Sector(String position, boolean dangerous, boolean alienStart, boolean humanStart){
		super(position);
		this.dangerous = dangerous;
		this.humanStart = humanStart;
		this.alienStart = alienStart;
		escapeHatch = 0;
	}
	
	//GETTERS
	private boolean isDangerous() {
		return dangerous;
	}
		
	private boolean isSecure() {
		return exists() && ( getEscapeHatch() == 0 ) && !isDangerous() && !isHumanStart() && !isAlienStart() ;
	}
	
	private boolean isHumanStart() {
		return humanStart;
	}
	
	private boolean isAlienStart() {
		return alienStart;
	}

	private boolean exists() {
		return exists;
	}

	public int getEscapeHatch() {
		return escapeHatch;
	}

	public boolean equals(Position pos) {
		if( getColumn() == pos.getColumn() && getRow() == pos.getRow() )
			return true;
		else return false;
	}
	
	
	public SectorProperty getType() {
		if( isDangerous() )
			return SectorProperty.DANGEROUS;
		else if( isSecure() )
			return SectorProperty.SECURE;
		else if ( getEscapeHatch() != 0 )
			return SectorProperty.ESCAPE_HATCH;
		else if( isHumanStart() )
			return SectorProperty.HUMAN_START;
		else if( isAlienStart() )
			return SectorProperty.ALIEN_START;
		else
			return SectorProperty.NOT_EXISTS;
	}
}
