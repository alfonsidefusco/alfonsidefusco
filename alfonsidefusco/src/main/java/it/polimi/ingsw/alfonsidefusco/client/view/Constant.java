package it.polimi.ingsw.alfonsidefusco.client.view;

enum Constant {
	//CliImp constants
	SKIP_SLASH(1), ENTIRE_LINE(95), ROW_OFFSET(2), COLUMN_OFFSET(4), INFO_LINES(2), END_FILE(-1),
	//Sectors constants
	TERMINATOR((int) ';'), SKIP_SEPARATOR(1);

	int value;
	
	//CONSTRUCTOR
	Constant(int value) {
		this.value = value;
	}
	
}
