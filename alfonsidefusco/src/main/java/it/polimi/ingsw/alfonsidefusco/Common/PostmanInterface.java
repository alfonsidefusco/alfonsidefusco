package it.polimi.ingsw.alfonsidefusco.Common;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface PostmanInterface extends Remote {

	public void send(String message) throws RemoteException ;

	public String ask(String question) throws RemoteException;
	
}