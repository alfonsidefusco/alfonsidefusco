package it.polimi.ingsw.alfonsidefusco.Common;

import java.io.Serializable;

import it.polimi.ingsw.alfonsidefusco.server.model.Position;

public interface PlayerInterface extends Serializable {

	public String getName();
		
	public Position lastKnownPosition();
	
	public String nature();
	
	public int objectCardsNumber();
	
}
