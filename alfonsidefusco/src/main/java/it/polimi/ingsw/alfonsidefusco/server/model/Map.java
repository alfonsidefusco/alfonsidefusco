package it.polimi.ingsw.alfonsidefusco.server.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import it.polimi.ingsw.alfonsidefusco.server.Exceptions.*;

public class Map {
	
	private String fileName;
	private Set<Sector> map = new HashSet<Sector>(336, (float) 0.05);

	//return a new reader from the fileName specified as argument
	private BufferedReader getReader(String fileName) {
	
		File file;
		String filePath;
		BufferedReader	reader = null;
		
		
		String workingDir = System.getProperty("user.dir");
		//Now build the path to the file of the map
		filePath = workingDir+"/resources/MapDefinitions/" + fileName;
		file = new File(filePath);
		try {
			reader = new BufferedReader(new FileReader(file));
			reader.readLine();
		}
		catch(IOException fileAccessDenied) {
			System.out.println("getReader: I/O Error: " + fileAccessDenied);
		}
		return reader; 
	}

	//return an object sector built from a String given as argument if well defined
	private static Position fetchSector(String line) throws IndexOutOfBoundsException {
		
		Sector sec;
		String position = null;
		char prop = 0;
		
		
		//fetch property character of the sector
		int i = line.length()-1;
		while( !Character.isLetterOrDigit( line.charAt(i) ) )
			i--;
		prop = line.charAt(i);
		
		//get coordinates position relative to the property
		position = line.substring(0, i);
		
		
		//generate Sector
		if( Character.isDigit(prop) ) {
			int escapeHatch = Character.getNumericValue(prop);
			sec = new Sector(position, escapeHatch);
		}
		else {
			prop = Character.toUpperCase(prop);
			switch(prop) {
			case 'D':
				sec = new Sector(position, true, false, false);
				break;
			case 'S':
				sec = new Sector(position, false, false, false);
				break;
			case 'A':
				sec = new Sector(position, false, true, false);
				break;
			case 'H':
				sec = new Sector(position, false, false, true);
				break;
			case 'N':
				sec = new Sector(position, false);
				break;
			default :
				sec = new Sector(position, false);
				break;
			}
		}
		return sec;
	}
	
	//debug method: print all sectors with their own properties
	/*public void verifySector() {
		for(Sector scc : map) {
			if (scc.isDangerous() )
				System.out.print("Dangerous ");
			else if( scc.isSecure() )
				System.out.print("Secure ");
			else if( scc.isAlienStart() )
				System.out.print("Alien Start ");
			else if ( scc.isHumanStart() )
				System.out.print("Human Start ");
			else if ( !scc.exists() )
				System.out.print("Not exists ");
			else if (scc.getEscapeHatch() != 0)
				System.out.print( scc.getEscapeHatch() + " ");
			System.out.println( scc.getColumn() + "" + scc.getRow() );
			System.out.print( "exists " +scc.exists() +
					"\ndangerous " +scc.isDangerous() +
					"\nsecure " + scc.isSecure() +
					"\nhumanstart " + scc.isHumanStart()+
					"\nalienstart " +scc.isAlienStart() +
					"\nescapeHatch number " + scc.getEscapeHatch() );
		}
	}
	*/
	
	
	public Sector findSector(Position pos) throws SectorNotFoundException {	
	
	boolean found = false;
	Sector wanted = null;
	for(Sector sec : map)
		if( !found && sec.equals(pos) ) {
			wanted = sec;
			found = true;
		}
	if(!found)
		throw new SectorNotFoundException();
	else
		return wanted;
}


	public SectorProperty checkSector(Position pos) throws SectorNotFoundException{
		
			Sector sec = findSector(pos);
			return sec.getType();
	}

	
	public Set<Sector> getSectors() {
		return map;
	}

	
 	public String name() {
 		return fileName;
 	}
	
 	//CONSTRUCTOR
	public Map(String fileName) {
		
		this.fileName = fileName;
		try {
			BufferedReader propertiesReader = getReader(fileName);
			String line = propertiesReader.readLine();
			do {
				try {
					map.add( (Sector) fetchSector(line) );
				}
				catch (IndexOutOfBoundsException e) {
				}
				line = propertiesReader.readLine();
			}			
			while( line != null);
		}
		catch (IOException e) {
			System.out.println("Failure in retrieving Map Definition file. Check given argument !");
		}
	}

}
