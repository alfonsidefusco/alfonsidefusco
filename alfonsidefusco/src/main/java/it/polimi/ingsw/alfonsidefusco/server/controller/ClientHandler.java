package it.polimi.ingsw.alfonsidefusco.server.controller;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

public class ClientHandler {
	
	private static ClientHandler instance;
	private List<ClientImage> clients = new ArrayList<ClientImage>();
	
	
	
	public void add(ClientImage client) {
		clients.add(client);
	}
	
	public void remove(ClientImage client) {
		clients.remove(client);
	}
	
	public ClientImage get(int index) {
		return clients.get(index);
	}
	
	public List<ClientImage> getAll() {
		return clients;
	}
	
	public void clean() {
		clients.clear();
	}
	
	public static ClientHandler getInstance() {
		if( instance == null )
			instance = new ClientHandler();
		return instance;
	}
	
	public void messageToAll(String message) {
		for(ClientImage client : clients)
			try {
				client.getPostman().send(message);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
	}

	
	
	private ClientHandler() {
		instance = this;
	}
}
