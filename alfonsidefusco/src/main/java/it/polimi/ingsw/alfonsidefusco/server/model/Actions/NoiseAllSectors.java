package it.polimi.ingsw.alfonsidefusco.server.model.Actions;

import java.rmi.RemoteException;

import it.polimi.ingsw.alfonsidefusco.server.model.Player;
import it.polimi.ingsw.alfonsidefusco.server.model.Position;
import it.polimi.ingsw.alfonsidefusco.server.Exceptions.*;
import it.polimi.ingsw.alfonsidefusco.server.model.SectorProperty;
import it.polimi.ingsw.alfonsidefusco.server.model.Match;


public class NoiseAllSectors implements Action{
	
	Position pos = null;

	public void action(Match callingMatch, Player actor) throws InvalidNoiseSectorException, RemoteException {
		checkNoiseSector(callingMatch, pos);
		actor.getClient().log( pos.toString() );
	}
	
	private void checkNoiseSector(Match callingMatch, Position show) throws InvalidNoiseSectorException {

		SectorProperty prop=null;
		try {
			prop= callingMatch.getMap().checkSector(show);
		} catch (SectorNotFoundException e) {
			throw new InvalidNoiseSectorException();
		}
		if(prop !=SectorProperty.DANGEROUS)
			throw new InvalidNoiseSectorException();
	
		
	}


	public void setTargetPosition(Position pos) {
		this.pos = pos;
	}

}


