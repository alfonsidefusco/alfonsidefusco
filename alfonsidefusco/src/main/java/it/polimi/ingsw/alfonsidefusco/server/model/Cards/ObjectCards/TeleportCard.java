package it.polimi.ingsw.alfonsidefusco.server.model.Cards.ObjectCards;

import it.polimi.ingsw.alfonsidefusco.server.model.Match;
import it.polimi.ingsw.alfonsidefusco.server.model.Player;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.Card;
import it.polimi.ingsw.alfonsidefusco.server.model.Actions.Teleport;

public class TeleportCard implements Card {
	private static int quantity = 2;
	public static final String name = new String("Teleport"); 
	
	@Override
	public boolean areFinished() {
		boolean finished = false;
		if (quantity == 0 )
			finished = true;
		return finished;	
	}

	
	@Override
	public void effect(Match match, Player player) throws Exception {
		new Teleport().action(match, player);
	}
	
	@Override
	public void deduct() {
		quantity--;
	}
	
	public String toString() {
		return name;
	}
}
