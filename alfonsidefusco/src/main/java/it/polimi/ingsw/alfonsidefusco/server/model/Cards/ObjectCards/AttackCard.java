package it.polimi.ingsw.alfonsidefusco.server.model.Cards.ObjectCards;

import java.util.ConcurrentModificationException;

import it.polimi.ingsw.alfonsidefusco.server.model.Match;
import it.polimi.ingsw.alfonsidefusco.server.model.Player;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.Card;
import it.polimi.ingsw.alfonsidefusco.server.model.Actions.Attack;


public class AttackCard implements Card {
	private static int quantity = 2;
	public static final String name = new String("Attack"); 
	
	
	@Override
	public boolean areFinished() {
		boolean finished = false;
		if (quantity == 0 )
			finished = true;
		return finished;	
	}

	
	@Override
	public void effect(Match match, Player player) throws ConcurrentModificationException {
		new Attack().action(match, player);
	}

	@Override
	public void deduct() {
		quantity--;
	}

	public String toString() {
		return name;
	}
	
}
