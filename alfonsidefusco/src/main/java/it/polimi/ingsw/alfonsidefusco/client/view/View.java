package it.polimi.ingsw.alfonsidefusco.client.view;

public interface View {
	
	public void showMsg(String msg);
	
	public String waitAnswer();

		
}
