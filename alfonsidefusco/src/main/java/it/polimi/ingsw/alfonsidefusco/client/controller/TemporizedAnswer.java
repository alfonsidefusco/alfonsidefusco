package it.polimi.ingsw.alfonsidefusco.client.controller;

public class TemporizedAnswer implements Runnable {
	private Postman instance;
	
	public void run() {
		instance.recipient = Postman.getInstance().listen();
		if(!instance.timeout)
			instance.answered = true;
	}
	
	TemporizedAnswer(Postman instance){
		this.instance = instance;
	}
}
