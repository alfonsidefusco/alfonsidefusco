package it.polimi.ingsw.alfonsidefusco.server.model.Actions;

import it.polimi.ingsw.alfonsidefusco.server.model.Match;
import it.polimi.ingsw.alfonsidefusco.server.model.Player;

public class Noise implements Action{
	
	public void action (Match partita, Player actor) throws Exception{
		actor.getClient().log( actor.getPosition().toString() );
	}

}