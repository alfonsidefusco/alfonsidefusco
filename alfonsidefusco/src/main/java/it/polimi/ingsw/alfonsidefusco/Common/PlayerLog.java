package it.polimi.ingsw.alfonsidefusco.Common;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface PlayerLog extends Remote {

	public void setName(String name) throws RemoteException;
	
	public void log(String publicPosition) throws RemoteException;
	
	public void log(String publicposition, String nature) throws RemoteException;
	
	public void log(int objectCards) throws RemoteException;
	
	public String getName() throws RemoteException;
	
}
