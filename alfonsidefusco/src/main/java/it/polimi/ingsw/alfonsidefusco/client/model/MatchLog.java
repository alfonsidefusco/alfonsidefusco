package it.polimi.ingsw.alfonsidefusco.client.model;

import it.polimi.ingsw.alfonsidefusco.Common.MatchInterface;
import it.polimi.ingsw.alfonsidefusco.Common.PlayerClientInterface;
import it.polimi.ingsw.alfonsidefusco.Common.ServerMatchInterface;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class MatchLog extends Observable implements MatchInterface, Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	private String mapName;
	private String rules;
	private int round = 1;
	private String currentPlayer = new String( "no one");
	private List<PlayerClientInterface> playersLog = new ArrayList<PlayerClientInterface>();
	private PlayerClient me;
	
	
	
	public int getRound() {
		return round;
	}
	
	public String getCurrentPlayer() {
		return currentPlayer;
	}
	
	public List<Rival> getPlayersLog() {
		List<Rival> rivals = new ArrayList<Rival>();
		for(PlayerClientInterface pl : playersLog ) {
			Rival rival = (Rival) pl;
			rivals.add(rival);
		}
		return rivals;
	}
	
	public void increaseRound() throws RemoteException {
		round++;
		setChanged();
		notifyObservers( new String("round") );
	}
	
	public void setCurrentPlayer(String playerName) throws RemoteException {
		currentPlayer = playerName;
		setChanged();
		if(currentPlayer.equals( me.getName()) )
			notifyObservers( new String("me") );
		else
			notifyObservers( new String("currentplayer") );
	}
	
	public String getMap() throws RemoteException {
		return mapName;
	}
	
	public String getRules() throws RemoteException {
		return rules;
	}
	

	public void update(ServerMatchInterface matchSkeleton) throws RemoteException {
		playersLog = matchSkeleton.downloadPlayersLog();
		setChanged();
		notifyObservers( new String("playerlog"));
	}
	
	@Override
	public void endGame() throws RemoteException {
		setChanged();
		notifyObservers( new String("endgame") );
	}
	
	public MatchLog(String mapName, String rules, PlayerClient me) {
		this.mapName = mapName;
		this.rules = rules;
		this.me = me;
	}
	
}
