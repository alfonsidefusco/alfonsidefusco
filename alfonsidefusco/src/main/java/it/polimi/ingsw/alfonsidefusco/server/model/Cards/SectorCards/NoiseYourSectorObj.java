package it.polimi.ingsw.alfonsidefusco.server.model.Cards.SectorCards;


import it.polimi.ingsw.alfonsidefusco.server.model.Match;
import it.polimi.ingsw.alfonsidefusco.server.model.Player;
import it.polimi.ingsw.alfonsidefusco.server.model.Actions.Noise;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.Card;

public class NoiseYourSectorObj extends NoiseWithObject implements Card{
	private static int quantity = 4;


	@Override
	public void effect(Match match, Player player) throws Exception {
		new Noise().action(match, player);
	}
	
	@Override
	public void deduct() {
		quantity--;
	}
	
	
	@Override
	public boolean areFinished() {
		boolean finished = false;
		if (quantity == 0 )
			finished = true;
		return finished;	
	}

}
