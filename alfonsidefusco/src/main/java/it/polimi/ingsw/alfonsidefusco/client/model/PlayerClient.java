package it.polimi.ingsw.alfonsidefusco.client.model;

import it.polimi.ingsw.alfonsidefusco.Common.PlayerClientInterface;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class PlayerClient extends Observable implements PlayerClientInterface, Rival  {
	

	private String name = new String("");
	private String position = new String("");
	private String publicPosition = new String("");
	private List<String> objectCards = new ArrayList<String>();
	private int objectCardsNumber = 0;
	private boolean dead = false;
	private boolean win = false;
	private int mobility = 0;
	private String nature = new String("unknown");
	
	
	@Override
	public String getName() {
		return name;
	}


	/* (non-Javadoc)
	 * @see it.polimi.ingsw.alfonsidefusco.client.model.PlayerClientInterface#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;
		}
	
	/* (non-Javadoc)
	 * @see it.polimi.ingsw.alfonsidefusco.client.model.PlayerClientInterface#addObjectCard(java.lang.String)
	 */
	@Override
	public void addObjectCard(String objectCard) throws RemoteException{
		objectCards.add(objectCard);
		objectCardsNumber++;
		setChanged();
		notifyObservers( new String("objects") );
	}
	
	public List<String> getObjectCards() {
		return objectCards;
	}

	@Override
	public int getObjectCardsNumber() {
		return objectCardsNumber;
	}
	
	@Override
	public void removeObjectCard(String objectCard) throws RemoteException {
		objectCards.remove(objectCard);
		objectCardsNumber--;
		setChanged();
		notifyObservers( new String("objects") );
	}
	
	/* (non-Javadoc)
	 * @see it.polimi.ingsw.alfonsidefusco.client.model.PlayerClientInterface#setDead(boolean)
	 */
	@Override
	public void setDead(boolean dead) throws RemoteException{
		this.dead = dead;
		setChanged();
		notifyObservers( new String("setdead") );
		}
	
	@Override
	public boolean isDead() throws RemoteException{
		return dead;	
	}
	
	/* (non-Javadoc)
	 * @see it.polimi.ingsw.alfonsidefusco.client.model.PlayerClientInterface#setWin(boolean)
	 */
	@Override
	public void setWin(boolean win) throws RemoteException{
		this.win = win;
		setChanged();
		notifyObservers( new String("setwin") );
		}

	@Override
	public boolean isWinner() throws RemoteException {
		return win;
	}

	public String getPosition() {
		return position;
	}
	
	/* (non-Javadoc)
	 * @see it.polimi.ingsw.alfonsidefusco.client.model.PlayerClientInterface#setPosition(it.polimi.ingsw.alfonsidefusco.server.model.Position)
	 */
	@Override
	public void setPosition(String position) throws RemoteException{
		this.position = position;
		setChanged();
		notifyObservers( new String("position") );
	}

	@Override
	public void setMobility(int mobility) throws RemoteException {
		this.mobility = mobility;
		setChanged();
		notifyObservers( new String("mobility") );
	}
	
	public int getMobility() {
		return mobility;
	}

	@Override
	public void log(String publicPosition) {
		this.publicPosition = publicPosition;
	}
	
	@Override
	public String getPublicPosition() {
		return publicPosition;
	}

	@Override
	public void log(String publicPosition, String nature) {
		this.publicPosition = publicPosition;
		this.nature = nature;
	}
	
	@Override
	public void logNature(String nature) throws RemoteException {
		this.nature = nature;
	}
	
	@Override
	public String getNature() {
		return nature;
	}
	
	@Override
	public void logDrawn() throws RemoteException {
		objectCardsNumber++;
		setChanged();
		notifyObservers( new String("objects") );
	}
	
	@Override
	public void logDiscard() throws RemoteException {
		objectCardsNumber--;
		setChanged();
		notifyObservers( new String("objects") );
	}
	
}
