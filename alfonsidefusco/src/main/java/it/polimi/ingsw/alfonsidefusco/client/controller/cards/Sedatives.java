package it.polimi.ingsw.alfonsidefusco.client.controller.cards;

public class Sedatives extends Card implements CardInterface {
	
	@Override
	public void interact() {
		screen.showMsg("Sedatives Card activated. You don't draw sector cards for this turn");
	}

}
