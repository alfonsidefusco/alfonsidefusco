package it.polimi.ingsw.alfonsidefusco.server.Exceptions;

public class SectorNotFoundException extends Exception{

	private static final long	serialVersionUID	= 1L;

	public SectorNotFoundException() {
		super();
	}
}

