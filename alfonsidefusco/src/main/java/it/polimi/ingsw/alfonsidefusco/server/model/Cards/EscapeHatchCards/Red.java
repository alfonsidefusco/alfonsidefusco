package it.polimi.ingsw.alfonsidefusco.server.model.Cards.EscapeHatchCards;

import it.polimi.ingsw.alfonsidefusco.server.model.Match;
import it.polimi.ingsw.alfonsidefusco.server.model.Actions.RedHatch;
import it.polimi.ingsw.alfonsidefusco.server.model.Player;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.Card;

public class Red implements Card {
	private static int quantity = 3;
	
	@Override
	public boolean areFinished() {
		boolean finished = false;
		if (quantity == 0 )
			finished = true;
		return finished;	
	}
		
	@Override
	public void effect(Match match, Player player) throws Exception {
		new RedHatch().action(match, player);
	}
	
	@Override
	public void deduct() {
		quantity--;
	}

}
