package it.polimi.ingsw.alfonsidefusco.server.controller;

import it.polimi.ingsw.alfonsidefusco.Common.MatchInterface;
import it.polimi.ingsw.alfonsidefusco.Common.PlayerClientInterface;
import it.polimi.ingsw.alfonsidefusco.Common.PostmanInterface;
import it.polimi.ingsw.alfonsidefusco.Common.ServerInterface;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Receiver implements ServerInterface {
	
	@SuppressWarnings("unused")
	private Registry serverRegistry;
	public MatchHandler provaMatch;
	MatchBinder binder = MatchBinder.getInstance();
	
	


	@Override
	public void register(int port) throws RemoteException {
	

		try {
			Registry registry = LocateRegistry.getRegistry(port);
			PostmanInterface postman = (PostmanInterface) registry.lookup("postman");
			PlayerClientInterface client = (PlayerClientInterface) registry.lookup("player");
			MatchInterface match = (MatchInterface) registry.lookup("match");

			
			binder.toBind(match, client, postman);
			Thread thr1 = new Thread(binder);
			thr1.start();
			
		}
		catch( RemoteException | NotBoundException e) {
			e.printStackTrace();
		}
	}

	
	
	Receiver(Registry registry) {
		serverRegistry = registry;
	}
	
	
}
