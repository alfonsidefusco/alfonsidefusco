package it.polimi.ingsw.alfonsidefusco.server.controller;

import it.polimi.ingsw.alfonsidefusco.Common.PostmanInterface;

public class ClientImage {

	private PostmanInterface postman;
	
	//used only during registration by Receiver
	public void setPostman(PostmanInterface postman) {
		this.postman = postman;
	}
	
	public PostmanInterface getPostman() {
		return postman;
	}
	
}
