package it.polimi.ingsw.alfonsidefusco.server.model.Cards.CharacterCards;

import it.polimi.ingsw.alfonsidefusco.server.model.Alien;
import it.polimi.ingsw.alfonsidefusco.server.model.Human;
import it.polimi.ingsw.alfonsidefusco.server.model.Player;

import java.util.ArrayList;
import java.util.List;

public class CharacterCards {
	
	//FIELDS
	List<Character> deck = new ArrayList<Character>();
	

	public Player draw() {
		int choice = (int) ( Math.random() * deck.size() );
		Character drawn = deck.get(choice);
		deck.remove(choice);
		Player player;
		if( drawn.nature.equalsIgnoreCase( new String("alien")) )
			player = new Alien(drawn.name);
		else
			player = new Human(drawn.name);
		return player;
	}

	
	private void shuffle() {
		String human = new String("human");
		String alien = new String("alien");
		deck.add(new Character(new String("Ennio Maria Dominoni"), human));
		deck.add(new Character(new String("Julia Niguloti a.k.a. “Cabal”"), human));
		deck.add(new Character(new String("Silvano Porpora"), human));
		deck.add(new Character(new String("Tuccio Brendon a.k.a. “Piri”"), human));
		deck.add(new Character(new String("Piero Ceccarella"), alien));
		deck.add(new Character(new String("Vittorio Martana"), alien));
		deck.add(new Character(new String("Maria Galbani"), alien));
		deck.add(new Character(new String("Paolo Landon"), alien));
	}
	
	public CharacterCards(int numberOfDecks) {
		while(numberOfDecks > 0) {
			shuffle();
			numberOfDecks--;
		}
	}
	
}
