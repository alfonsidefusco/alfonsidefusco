package it.polimi.ingsw.alfonsidefusco.server.model.Cards.EscapeHatchCards;

import it.polimi.ingsw.alfonsidefusco.server.model.Match;
import it.polimi.ingsw.alfonsidefusco.server.model.Actions.GreenHatch;
import it.polimi.ingsw.alfonsidefusco.server.model.Player;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.Card;

public class Green implements Card {
	private static int quantity = 3;
	public static String name = "Green Escape Hatch";
			
	@Override
	public boolean areFinished() {
		boolean finished = false;
		if (quantity == 0 )
			finished = true;
		return finished;	
	}
	
	@Override
	public void effect(Match match, Player player) throws Exception {
		new GreenHatch().action(match, player);
	}
	
	@Override
	public void deduct() {
		quantity--;
	}

}
