package it.polimi.ingsw.alfonsidefusco.Common;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface PlayerClientInterface extends Remote{

public void setName(String name) throws RemoteException;

public void addObjectCard(String objectCard) throws RemoteException;

public void removeObjectCard(String objectCard) throws RemoteException;

public void setDead(boolean dead) throws RemoteException;

public void setWin(boolean win) throws RemoteException;

public void setPosition(String position) throws RemoteException;

public void setMobility(int mobility) throws RemoteException;

public void log(String publicPosition) throws RemoteException;

public void log(String publicposition, String nature) throws RemoteException;

public void logDrawn() throws RemoteException;

public void logDiscard() throws RemoteException;

public void logNature(String nature) throws RemoteException;

}