package it.polimi.ingsw.alfonsidefusco.server.model.Actions;

import it.polimi.ingsw.alfonsidefusco.server.model.Match;
import it.polimi.ingsw.alfonsidefusco.server.model.Player;
import it.polimi.ingsw.alfonsidefusco.server.model.Sector;
import it.polimi.ingsw.alfonsidefusco.server.model.SectorProperty;



public class Teleport implements Action{
	public void action(Match callingMatch, Player actor) throws Exception{
		for(Sector tele : callingMatch.getMap().getSectors())
			if(tele.getType()==SectorProperty.HUMAN_START){
				actor.setPosition(tele);
				return;
			}
		
	}
	
}