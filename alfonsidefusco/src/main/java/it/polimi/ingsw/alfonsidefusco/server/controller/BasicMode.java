package it.polimi.ingsw.alfonsidefusco.server.controller;

import it.polimi.ingsw.alfonsidefusco.server.Exceptions.*;
import it.polimi.ingsw.alfonsidefusco.server.model.Alien;
import it.polimi.ingsw.alfonsidefusco.server.model.Human;
import it.polimi.ingsw.alfonsidefusco.server.model.Player;
import it.polimi.ingsw.alfonsidefusco.server.model.Position;
import it.polimi.ingsw.alfonsidefusco.server.model.SectorProperty;
import it.polimi.ingsw.alfonsidefusco.server.model.Actions.Attack;
import it.polimi.ingsw.alfonsidefusco.server.model.Actions.Move;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.Card;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.SectorCards.NoiseAllSector;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.SectorCards.NoiseAllSectorObj;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.SectorCards.Silence;

import java.rmi.RemoteException;
import java.util.ConcurrentModificationException;
import java.util.Scanner;


public class BasicMode extends MatchHandler {

	Scanner input = new Scanner(System.in);
	
	//FIELDS
	final int HUMAN_MAX_DISTANCE = 1;
	final int ALIEN_MAX_DISTANCE = 2;
	final static int MAX_ROUNDS = 39;
	
	//METHODS
	public void humanTurn(Human pl) throws RemoteException {
		SectorProperty dest = null;
		
		dest = moveWrap(pl);

		if(dest == SectorProperty.ESCAPE_HATCH ) {
			pl.setEscaped();
			endGame = true;
		}
		else if (dest == SectorProperty.DANGEROUS ) {
		
				drawSectorCard(pl);

			
			
		}			
	}
	

	public void alienTurn(Alien pl) throws RemoteException{
		SectorProperty dest = null;

			dest = moveWrap(pl);

		String keyword = "attack";
		String choice = null;
		
			choice = pl.getPostman().ask("You can launch an attack in your current position by typing <" + keyword + ">, or you can keep hiding your real identity to other players (just type any key)" );

		if( choice.equals(keyword) )
			attackWrap(pl);
		else 
			if( dest == SectorProperty.DANGEROUS ) {
			
					drawSectorCard(pl);
		
			}
	}
	

	public void win() {
		// I case in handled humanTurn()>setEscaped()
		// II case
		if (match.getWinners().isEmpty() ) {
			boolean noHumans = true;
			for(Player pl: match.getPlayers())
				if( pl instanceof Human )
					noHumans = false;
			if(noHumans)
				match.setAliensWinners();
		}
		// III case
		else if( match.getRound() == MAX_ROUNDS ) {
			boolean escaped = false; 
			for( Player pl: match.getAllPlayers() )
				if( pl.isWinner() )
					escaped = true;
			if(!escaped)
				match.setAliensWinners();
		}
		//EndGame condition
		if(	!match.getWinners().isEmpty() )
			endGame = true;
	}

	
	Card drawSectorCard(Player pl) throws RemoteException {
		Card card =  match.getSectorCard().draw();
		if(! (card instanceof NoiseAllSectorObj || card instanceof NoiseAllSector) ){
			try {
				card.effect(match, pl );
				if(card instanceof Silence)
					sendToAll( pl.getCharacter() + " declares silence");
				else
					sendToAll(pl.getCharacter() + ": Noise in " + pl.getPosition().toString() );
			} catch (Exception e) {
				e.printStackTrace();
			}
			return card;
		}
		else if( card instanceof NoiseAllSectorObj )
			pl.getPostman().send("Noise in all sectors with object card drawn.");
		else if (card instanceof NoiseAllSector )
			pl.getPostman().send("Noise in all sectors card drawn.");
		System.out.println("card noise all");
		boolean ok = false;
		String dest = null;
		Position target;
		while(!ok) {
			
			dest = pl.getPostman().ask("You must specify a dangerous sector for this card.");
			target = new Position( dest );
			noiseAllSectorDecorator(card, target);
			try {
				card.effect(match, pl);
				sendToAll(pl.getCharacter() + ": Noise in " + target.toString() );
				ok = true;
			}catch (Exception e) {
				if(e instanceof InvalidNoiseSectorException) {
					pl.getPostman().send("Invalid input, lease specify only a dangerous sector");
					continue;
				}
			} 
		}
		
	return card;
	}
	
	
	private void noiseAllSectorDecorator(Card card, Position target){
		if( card instanceof NoiseAllSectorObj ) 
			((NoiseAllSectorObj) card ).setTargetPosition(target);
		
		else if (card instanceof NoiseAllSector ) 
			((NoiseAllSector) card ).setTargetPosition(target);
		
	}
	
	
	SectorProperty moveWrap(Player pl) throws RemoteException {
	
		Move move = new Move();
		String dest = null;
		Position destination;
		boolean success = false;
		while(!success) {
			dest = pl.getPostman().ask("Insert move destination");
			destination = new Position( dest );
			move.setTargetPosition(destination);
			try {
				move.action(match, pl);
				success = true;
			} catch (SectorNotFoundException e) {
				pl.getPostman().send("Settore non trovato");
				continue; 
			} catch (OutOfBoundsException e) {
				pl.getPostman().send("Ti puoi muovere di al massimo di un numero di setori pari a "+ pl.getMobility());
				continue; 
			} catch (StupidUserException e) {
				pl.getPostman().send("Posizione non valida!");
				continue; 
			} catch (EscapeHatchClosedException e) {
				pl.getPostman().send("This escape hatch is closed :( !");
				continue; 
			}
		}
		return pl.getPosition().getType();
	}
	
	
	void attackWrap(Alien aln) {
	
		for(Player pl : match.getPlayers() )
			if(pl!=aln)
				try {
					pl.getPostman().send( aln.getCharacter() + " attacks in " + aln.getPosition().toString() );
				} catch (RemoteException e1) {
					e1.printStackTrace();
				}
		Attack attack = new Attack();
		try {
			attack.action(match, aln);
		} catch (ConcurrentModificationException e) {}
		catch (Exception e) {
			e.printStackTrace();
		}
				
	}
	
	@Override
	void setInitialMobility() {
		for(Player pl : match.getPlayers() )
			if( pl instanceof Alien)
				pl.setMobility(ALIEN_MAX_DISTANCE);
			else pl.setMobility(HUMAN_MAX_DISTANCE);
	}
	
	BasicMode() {
		super.rules = new String("basic");
	}
}