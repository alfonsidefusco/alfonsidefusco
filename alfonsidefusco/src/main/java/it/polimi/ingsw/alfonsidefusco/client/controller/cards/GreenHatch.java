package it.polimi.ingsw.alfonsidefusco.client.controller.cards;

public class GreenHatch extends Card implements CardInterface {
	
	@Override
	public void interact() {
		screen.showMsg("Trying to access this lifeboat..... It's OK, it 'll lead you to safety");
	}

}
