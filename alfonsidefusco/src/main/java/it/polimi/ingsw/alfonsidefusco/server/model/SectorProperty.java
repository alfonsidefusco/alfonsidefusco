package it.polimi.ingsw.alfonsidefusco.server.model;

public enum SectorProperty {
	NOT_EXISTS('N'), DANGEROUS('D'), SECURE('S'), HUMAN_START('H'), ALIEN_START('A'), ESCAPE_HATCH('E');
	
	char property;

	
	//CONSTRUCTOR
 	SectorProperty(char prop){
		property = prop;
	}
}
