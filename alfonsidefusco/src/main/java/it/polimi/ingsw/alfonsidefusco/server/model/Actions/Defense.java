package it.polimi.ingsw.alfonsidefusco.server.model.Actions;

import java.util.Observable;
import java.util.Observer;

import it.polimi.ingsw.alfonsidefusco.server.model.Match;
import it.polimi.ingsw.alfonsidefusco.server.model.Player;

public class Defense implements Action, Runnable, Observer {

	Player pl;
	boolean updated = false;
	
	public void action(Match callingMatch, Player actor) throws Exception{
		actor.setInvincible(true);
		pl = actor;
	}


	@Override
	public void update(Observable obs, Object arg) {
		if((Player) obs == pl && ((String) arg).equals("invincibletrue") ) {
			pl.setInvincible(false);
			updated = true;
		}
	}
	
	
	public void run() {
		pl.addObserver(this);
		while(!updated) {
			try {
				Thread.sleep(3000);       //1000 milliseconds is one second.
			} catch(InterruptedException ex) {
			    Thread.currentThread().interrupt();
			}
		}	
		pl.deleteObserver(this);
		return ;
	}
}