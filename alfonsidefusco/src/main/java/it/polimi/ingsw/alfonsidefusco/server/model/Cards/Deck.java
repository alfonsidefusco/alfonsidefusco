package it.polimi.ingsw.alfonsidefusco.server.model.Cards;

import java.util.ArrayList;
import java.util.List;

public abstract class Deck {
	
	private List<Card> deck = new ArrayList<Card>();

	public Card draw() {
	
		deck = getDeck();
		if(deck.isEmpty())
			shuffle();
		int choice = (int) ( Math.random() * deck.size() );
		Card drawn = deck.get(choice);
		drawn.deduct();
		
		if( drawn.areFinished() )
			deck.remove(choice);
		
		return drawn;
	}
	
	
	protected abstract List<Card> getDeck();
	
	
	//This refill pack with a card object for each type of card. Each inheriting class must define which cards to add to its pack within this method.
	protected abstract void shuffle();
			
}
