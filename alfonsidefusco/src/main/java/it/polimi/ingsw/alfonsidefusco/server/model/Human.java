package it.polimi.ingsw.alfonsidefusco.server.model;

import it.polimi.ingsw.alfonsidefusco.server.Exceptions.ReachObjectCardsLimitException;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.Card;

import java.rmi.RemoteException;
import java.util.Collections;


public class Human extends Player {
	
	
	private boolean escaped = false;
	private boolean	sedative	= false;
	

	public boolean isEscaped() {
		return escaped;
	}


	public void setEscaped() {
		escaped = true;
		setWinner();
	}


	public void setSedative(boolean sedative) {
		this.sedative=sedative;
	}
	
	
	public boolean hasSedative() {
		return sedative;
	}

	@Override
	public void addObject(Card card) throws ReachObjectCardsLimitException {
		if( object.size() == OBJECT_CARDS ) 
			throw new ReachObjectCardsLimitException();
		else {
			object.add(card);
			try {
				client.addObjectCard(card.toString());
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void removeObject(int cardNumber) {
		Card card = object.get(cardNumber);
		object.remove(cardNumber);
		try {
			client.removeObjectCard( card.toString() );
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		object.removeAll(Collections.singleton(null));
	}

	@Override
	public void removeObject(Card card) {
		object.remove(card);
		try {
			client.removeObjectCard( card.toString() );
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		object.removeAll(Collections.singleton(null));
	}
	
	//CONSTRUCTOR
	public Human(String name ) {	
		setCharacter(name);
	}
}
