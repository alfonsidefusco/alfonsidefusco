package it.polimi.ingsw.alfonsidefusco.server.controller;

import it.polimi.ingsw.alfonsidefusco.Common.MatchInterface;
import it.polimi.ingsw.alfonsidefusco.server.model.Alien;
import it.polimi.ingsw.alfonsidefusco.server.model.Human;
import it.polimi.ingsw.alfonsidefusco.server.model.Match;
import it.polimi.ingsw.alfonsidefusco.server.model.Player;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.Card;

import java.rmi.RemoteException;

public abstract class MatchHandler implements Runnable {

	public Match match;
	public String rules;
	boolean endGame = false;
	boolean running = false;
	boolean started = false;
	
	

	public void handleTurns() {
		match.setStartPosition();
		setInitialMobility();
		updateClients();
		while (!endGame) {
			for( Player pl : match.getPlayers() ) {
				if( !( pl.isDead() || pl.isWinner() || pl.isDisconnected() ) && !endGame) {

					match.setCurrentPlayer(pl);

					
					if( !pl.getObjects().isEmpty() ) {
						try {
							pl.getPostman().send("Object Cards:");
						} catch (RemoteException e) {}
						for(Card card : pl.getObjects() )
							try {
								pl.getPostman().send( card.toString() );
							} catch (RemoteException e) {}
					}
					
					try {
						pl.getPostman().send("you are in " + pl.getPosition().getColumn() + "" + pl.getPosition().getRow() );
					} catch (RemoteException e) {}
					try {
						if (pl instanceof Human)
							humanTurn((Human) pl);
						else
							alienTurn((Alien) pl);
					}
					catch( RemoteException e) {
						sendToAll(pl.getCharacter() + " has been disconnected due to inactivity.");
						pl.setDisconnected();
						pl.setDead();
					}
				}
				win();
				if(!endGame)
					updateClients();
			}
			match.increaseRound();
		}
		System.out.println("game ended, someone has win");
	}

	
	public void sendToAll(String message) {
		for(Player pl : match.getAllPlayers() )
			try {
				pl.getPostman().send(message);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	
	public void run() {
		start();
		handleTurns();
		updateClients();
		match.endGame();
		MatchGenerator.getInstance().removeMatch(match);
	}
	
	
	abstract void humanTurn(Human pl) throws RemoteException;
	
	
	abstract void alienTurn(Alien pl) throws RemoteException;
	
	
	abstract void win();
	
	
	abstract void setInitialMobility();

	
	private void updateClients() {
		try {
			for(MatchInterface client : match.matchClient )
				client.update(match.serverMatch);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
	
	private void start() {
		running = true;
		final int COUNTDOWN = 10;
		final int REFRESH = 5000; //each 5 seconds
		int i =COUNTDOWN;
		while(!match.areHumans()) {
			try {
				Thread.sleep(3000); //3 seconds
			} catch (InterruptedException e) {
				e.printStackTrace();
			} //3000 milliseconds
		}
		while(i>0) {
			if(match.getAllPlayers().size() == match.MAX_PLAYERS )
				return;
			else {
				sendToAll("Game will start in " + i + "s");
				try {
					Thread.sleep(REFRESH);
				} catch (InterruptedException e) {
					continue;
				}
				i=i-REFRESH/1000;
			}
		}
		started = true;
		return;
	}
	
	
	boolean alreadyRunning() {
		return running;
	}
	
	boolean alreadyStarted() {
		return started;
	}
}
