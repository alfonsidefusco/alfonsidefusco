package it.polimi.ingsw.alfonsidefusco.server.model;

import it.polimi.ingsw.alfonsidefusco.Common.PlayerClientInterface;
import it.polimi.ingsw.alfonsidefusco.Common.PostmanInterface;
import it.polimi.ingsw.alfonsidefusco.server.Exceptions.ReachObjectCardsLimitException;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.Card;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;



public abstract class Player extends Observable {

	Sector position;
	String character;
	private boolean invincible = false;
	private int mobility = 1;
	public final int OBJECT_CARDS = 3;
	List<Card> object = new ArrayList<Card>(3);
	private boolean visible = false;
	private boolean dead = false;
	private boolean winner = false;
	private PostmanInterface postman;
	PlayerClientInterface client;
	boolean disconnected = false;
	//CharacterCard
	//id: String
	public void setCharacter(String name) {
		character = name;
	}


	public String getCharacter() {
		return character;
	}
	
	public boolean isInvincible() {
		return invincible;
	}


	public boolean isDead() {
		return dead;
	}


	public void setDead() {
		if( !invincible ) {
			dead = true;
			try {
				client.logNature( getNature() );
				client.setDead(true);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
		else {
			setChanged();
			notifyObservers( new String("invincibletrue") );
		}
	}

	public void setDisconnected() {
		disconnected = true;
	}
	
	public boolean isDisconnected() {
		return disconnected;
	}

	
	public boolean isWinner() {
		return winner;
	}


	public void setWinner() {
		winner = true;
		try {
			client.logNature( getNature() );
			client.setWin(true);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}


	public void setInvincible(boolean invincible) {
		this.invincible = invincible;
		System.out.println("invinvible set to " + invincible );
	}	
	
	@Deprecated
	public boolean isVisible() {
	return visible;
	}

	@Deprecated
	public void setVisible(boolean visible) {
	this.visible = visible;
	}


	public void setPosition(Sector position) {
		this.position = position;
		setChanged();
		notifyObservers(position);
		try {
			client.setPosition(position.toString());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public Sector getPosition() {
		return position;
	}
	
	
	public void setMobility(int mobility) {
		this.mobility = mobility;
		try {
			client.setMobility(mobility);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public int getMobility() {
		return mobility;
	}
	
	
	public abstract void addObject(Card card) throws ReachObjectCardsLimitException;
	
	
	public abstract void removeObject(Card card);
	
	
	public abstract void removeObject(int cardNumber);

	
	public List<Card> getObjects(){
		return (List<Card>) object;
	}


	public PostmanInterface getPostman() {
		return postman;
	}


	public void setPostman(PostmanInterface postman) {
		this.postman = postman;
	}

	
	public void setClient(PlayerClientInterface client) {
		this.client = client;
		try {
			client.setName(character);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public PlayerClientInterface getClient() {
		return client;
	}
	
	
	//METHODS MANAGING PUBLIC ASPECTS OF THIS PLAYER

	public String getNature() {
		String nature;
		if(this instanceof Human)
			nature = new String("human");
		else
			nature = new String("alien");
		return nature;
	}


}
