package it.polimi.ingsw.alfonsidefusco.server.Exceptions;

public class EscapeHatchClosedException extends Exception{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean blocked= true;
	public EscapeHatchClosedException(){
	}
	boolean getBlocked(){
		return blocked;
	}
}