package it.polimi.ingsw.alfonsidefusco.client.controller;

import it.polimi.ingsw.alfonsidefusco.Common.MatchInterface;
import it.polimi.ingsw.alfonsidefusco.Common.PlayerClientInterface;
import it.polimi.ingsw.alfonsidefusco.Common.PostmanInterface;
import it.polimi.ingsw.alfonsidefusco.Common.ServerInterface;
import it.polimi.ingsw.alfonsidefusco.client.model.MatchLog;
import it.polimi.ingsw.alfonsidefusco.client.model.PlayerClient;
import it.polimi.ingsw.alfonsidefusco.client.view.Cli;
import it.polimi.ingsw.alfonsidefusco.client.view.Gui;
import it.polimi.ingsw.alfonsidefusco.client.view.View;

import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Observer;
import java.util.Scanner;

public class Client {

	private Registry registry = null;
	private final int SERVER_PORT = 3030;
	private Registry serverRegisrty; 
	private static int port;
	private ServerInterface session;
	View vista;
	PlayerClient playerClient;
	MatchLog match;

	public static void main(String[] args) {
		new Client();
	}
	
	
	private void main2() {
	
		
		
		
		String mapChoice = chooseMap_Interface();
		
		playerClient = new PlayerClient();
		
		//ask rules
		String basic = new String("basic");
		String complete = new String("complete");
		String rulesChoice = new String("");
		while(!( rulesChoice.equalsIgnoreCase(basic) || rulesChoice.equalsIgnoreCase(complete) ) ) {
			vista.showMsg("Choose game rules between " + basic + " rules " + "or " + complete + " rules.");
			rulesChoice = vista.waitAnswer();
		}
		match = new MatchLog(mapChoice, rulesChoice, playerClient);

		
		
		createRegistry();
		boundPostman();
		boundMatch();
		boundPlayerClient();
		registerOnServer();
		
		playerClient.addObserver((Observer) vista);
		match.addObserver( (Observer) vista );
		
	}
	
	private void createRegistry() {
		
		port = 2020;
		boolean availablePort = false;
	
		while(!availablePort) {
			try {
				registry = LocateRegistry.createRegistry(port);
				availablePort = true;
			}
			catch( RemoteException e ) {
				availablePort = false;
				port++;
			}
		}
		
	}

	
	private String chooseMap_Interface() {
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		System.out.println("Which user interface do you want to use between cli or gui ? [cli/gui]");
		String userInterface = input.next();
		
		//ask playground
		String galilei = new String("galilei");
		String fermi = new String("fermi");
		String galvani = new String("galvani");
		String mapChoice = new String("");
		while(!( mapChoice.equalsIgnoreCase(galilei) || mapChoice.equalsIgnoreCase(galvani) || mapChoice.equalsIgnoreCase(fermi) )) {
			System.out.println("Choose you playground between " + galilei + ", " + fermi + " and " + galvani +".");
			mapChoice = input.next();
		}
		
		if( userInterface.equalsIgnoreCase( new String("cli") ))
			vista = new Cli(mapChoice);
		else 
			vista = new Gui(mapChoice);
		return mapChoice;
		}
	
	
	private void boundPostman() {
		Postman postman = Postman.getInstance();
		try {
			PostmanInterface postmanStub = (PostmanInterface) UnicastRemoteObject.exportObject(postman, 0);
			registry.bind("postman", postmanStub);
		} catch(RemoteException | AlreadyBoundException e) {
			System.out.println("Unable to exchange messages. Please restart.");
		}
	}

	private void boundMatch() {

		try {
			MatchInterface matchStub = (MatchInterface) UnicastRemoteObject.exportObject(match, 0);
			registry.bind("match", matchStub);
		} catch(RemoteException | AlreadyBoundException e) {
			System.out.println("Unable to connect to Match Generator. Please restart.");
		}
	}
	
	private void boundPlayerClient() {

		try {
			PlayerClientInterface playerClientStub = (PlayerClientInterface) UnicastRemoteObject.exportObject(playerClient, 0);
			registry.bind("player", playerClientStub);
		} catch(RemoteException | AlreadyBoundException e) {
			System.out.println("Unable to connect to Match Generator. Please restart.");
		}
	}

	
	
	
	private void registerOnServer() {
	

		try {
			serverRegisrty = LocateRegistry.getRegistry(SERVER_PORT);
			session = (ServerInterface) serverRegisrty.lookup("server");
			session.register(port);
		}
		catch( RemoteException | NotBoundException e) {
			System.out.println("Unable to connect to server. Please restart.");
		}
	}
	
	Client(){
		main2();
	}
}
