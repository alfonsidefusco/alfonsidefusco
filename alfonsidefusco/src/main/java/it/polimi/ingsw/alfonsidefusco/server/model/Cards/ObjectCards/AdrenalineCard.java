package it.polimi.ingsw.alfonsidefusco.server.model.Cards.ObjectCards;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import it.polimi.ingsw.alfonsidefusco.server.model.Match;
import it.polimi.ingsw.alfonsidefusco.server.model.Actions.Adrenaline;
import it.polimi.ingsw.alfonsidefusco.server.model.Player;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.Card;

public class AdrenalineCard implements Card {
	private static int quantity = 2;
	public static final String name = new String("Adrenaline"); 
	
	@Override
	public boolean areFinished() {
		boolean finished = false;
		if (quantity == 0 )
			finished = true;
		return finished;	
	}

	@Override
	public void effect(Match match, Player player) throws Exception {
		Adrenaline act = new Adrenaline();
		//Thread thr = new Thread(act);
		Executor ex = Executors.newCachedThreadPool();
		act.action(match, player);
		ex.execute(act);
		//thr.start();
		ex = null;
		act = null;	
	}
	
	@Override
	public void deduct() {
		quantity--;
	}
	
	public String toString() {
		return name;
	}
}
