package it.polimi.ingsw.alfonsidefusco.server.controller;

import it.polimi.ingsw.alfonsidefusco.Common.ServerInterface;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;

public class Server {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) throws RemoteException, AlreadyBoundException {
		

			Registry registry = LocateRegistry.createRegistry(3030);
		
			
			Receiver services = new Receiver(registry);
			ServerInterface servStub = (ServerInterface) UnicastRemoteObject.exportObject(services, 0);
			registry.bind("server", servStub);
			
			while(true)
				try {
					Thread.sleep(1000000000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			
			
				
			
			
			
			
	}
}
