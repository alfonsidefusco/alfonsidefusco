package it.polimi.ingsw.alfonsidefusco.server.model.Cards.SectorCards;

import java.rmi.RemoteException;

import it.polimi.ingsw.alfonsidefusco.server.Exceptions.InvalidNoiseSectorException;
import it.polimi.ingsw.alfonsidefusco.server.model.Match;
import it.polimi.ingsw.alfonsidefusco.server.model.Player;
import it.polimi.ingsw.alfonsidefusco.server.model.Position;
import it.polimi.ingsw.alfonsidefusco.server.model.Actions.NoiseAllSectors;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.Card;

public class NoiseAllSector implements Card{
	private static int quantity = 6;
	Position target;
 
	@Override
	public void effect(Match match, Player player) throws InvalidNoiseSectorException, RemoteException {
		NoiseAllSectors noise = new NoiseAllSectors();
		noise.setTargetPosition(target);
		noise.action(match, player);
	}
	
	
	@Override
	public void deduct() {
		quantity--;
	}
	
	
	public void setTargetPosition(Position pos) {
		this.target = pos;
	}
	
	
	@Override
	public boolean areFinished() {
		boolean finished = false;
		if (quantity == 0 )
			finished = true;
		return finished;	
	}

}
