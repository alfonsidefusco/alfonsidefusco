package it.polimi.ingsw.alfonsidefusco.server.model.Actions;
import java.rmi.RemoteException;

import it.polimi.ingsw.alfonsidefusco.server.Exceptions.IllegalSectorException;
import it.polimi.ingsw.alfonsidefusco.server.Exceptions.SectorNotFoundException;
import it.polimi.ingsw.alfonsidefusco.server.model.Match;
import it.polimi.ingsw.alfonsidefusco.server.model.Player;
import it.polimi.ingsw.alfonsidefusco.server.model.Position;
import it.polimi.ingsw.alfonsidefusco.server.model.SectorProperty;


public class SpotLight implements Action{
	
	private Match callingMatch;	
	private Position pos= new Position();
	
	@Override
	public void action(Match match, Player player) throws SectorNotFoundException, IllegalSectorException, RemoteException {
		callingMatch=match;
		showSpot(pos, player);	
	}
	

	private void showSpot(Position check, Player player) throws IllegalSectorException, SectorNotFoundException, RemoteException {
	
		SectorProperty prop = callingMatch.getMap().checkSector(check);
		
		if(prop==SectorProperty.NOT_EXISTS || prop==SectorProperty.ESCAPE_HATCH)
			throw new IllegalSectorException();
		else{
		Position enemyPos= new Position();
		Player enemy;
		Move find= new Move();
		boolean foundSomeone=false;
		
		for(int i=-1; i<6; i++){
			if(i>=0)
			check=find.neighbor(pos, i);
			for(int j=0; j<callingMatch.getPlayers().size(); j++){
				enemy=callingMatch.getPlayers().get(j);
				enemyPos=enemy.getPosition();
				if(enemyPos.equals(check)){
					foundSomeone=true;
					enemy.getClient().log(enemyPos.toString() );
				}
			}
			
		}
		
		if(!foundSomeone)
			try {
				player.getPostman().send("Unfortunately nobody was in the sector you chose");
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}

	public void setPos(Position pos) {
		this.pos = pos;
	}

}



