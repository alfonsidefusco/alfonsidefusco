package it.polimi.ingsw.alfonsidefusco.server.model.Actions;

import it.polimi.ingsw.alfonsidefusco.server.model.Match;
import it.polimi.ingsw.alfonsidefusco.server.model.Player;

public interface Action {

	public void action(Match match, Player player) throws Exception;
	
}
