package it.polimi.ingsw.alfonsidefusco.client.controller;

import it.polimi.ingsw.alfonsidefusco.Common.PostmanInterface;
import it.polimi.ingsw.alfonsidefusco.client.view.View;

import java.rmi.RemoteException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;



public class Postman implements PostmanInterface {

	private static View screen;
	private static Postman instance;
	private Executor ex = Executors.newCachedThreadPool();
	boolean timeout;
	boolean answered;
	String recipient;
	
	//METHODS AVAILABLE TO SERVER THROUGH POSTMAN INTERFACE
	@Override
	public void send(String message) throws RemoteException{
		print(message);
	}

//	@Override
//	public String ask(String question) throws RemoteException {
//		print(question);
//		String result = listen();
//	return result;
//	}
	
	@Override
	public String ask(String question) throws RemoteException {
		print(question);
		
		timeout = false;
		answered = false;
		final int refreshRate = 100; //0.1 seconds
		
		TemporizedAnswer temp = new TemporizedAnswer(this);
		Timer timer = new Timer(this);
		
	
		ex.execute(temp);
		ex.execute(timer);
		while(true) {
			try {
				Thread.sleep(refreshRate);
			} catch (InterruptedException e) {	}
			if(answered)
				 break;
			else if(timeout) 
				throw new RemoteException();	
		}
		return recipient;
	}
	
	
	public static Postman getInstance() {
		if( instance == null)
			instance = new Postman();
		return instance;
	}

	
	
	
	
	//select user interface (view)
	public void setView(View screenview) {
		screen = screenview;
		}

	
	
	//TOOLS
	//Print a message in selected view
	private void print(String message) {
		screen.showMsg(message);
		//System.out.println(message);
	}
	
	//Catch user text input from selected view
	String listen() {
		return screen.waitAnswer();
		//return input.next();
	
	}
	
}
