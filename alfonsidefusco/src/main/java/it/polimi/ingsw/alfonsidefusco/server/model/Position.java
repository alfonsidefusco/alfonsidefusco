package it.polimi.ingsw.alfonsidefusco.server.model;

import java.io.Serializable;

/**The Position class describe a coordinate given throug a character and a number.
 * In reference to a playground, a position object describes the coordinates of a sector but not any
 * other supplementary information, so that it's indipendent from the map in question. 
 * These parameters aren't changeable once the object has been created.
 *  * 
 * @author Tommy
 *
 */
public class Position implements Serializable {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;
	private char ch;
	private int chNumber; //it express the numeric value corresponding to ch in alphabeti order starting count from 0
	private int num = 0;
	private final String abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
	/**
	 * Initialize a position object given a String representing it even without a precise format.
	 * Whitespaces are ignored and only the first character and the first number fifferent from 0 are considered. It works
	 * with both uppercase and lowercase characters.
	 * @param String coordinates<p>		example "B2" or "B02"
	 *@return Position
	 **/
	public Position(String coord) {
		
		int i = 0;
		
		//skips whitespaces
		while( !Character.isLetter( coord.charAt(i) ) )
			i++;
		
		//get a single character
		ch = Character.toUpperCase( coord.charAt(i) );
		
		//skips whitespaces
		while( !Character.isDigit( coord.charAt(i) ) )
			i++;
		
		//get number(s)
		for(num=0 ; i < coord.length() && Character.isDigit(coord.charAt(i)); i++) {
			num = num*10 +  Character.getNumericValue( coord.charAt(i) );
		}
		
		for(chNumber=0; ch!=abc.charAt(chNumber); chNumber++);
	}
	
	/**
	 * Initialize a Position object by directly giving the column number and the row number.
	 * @param col The column number
	 * @param row The row number
	 * @return Position
	 */
	public Position(int col, int row){
		this.chNumber=col;
		this.num=row;
		this.ch=abc.charAt(col);
		
		
	}
	
	public int getRow() {
		return num;
	}
	
	
	public int getColumnInt() {
		return chNumber;
	}
	
	public char getColumn() {
		return ch;
	}
	
	/**Return a blank position object with column an row number set to 0
	 * 
	 */
	public Position(){
		this.ch=0;
		this.chNumber=0;
		this.num=0;
		
	}
	
	/**
	 * 
	 * Compares two Position object and return true if their column and row are identical.
	 * 
	 * @param pos - The position to be compared with this object.
	 * @return true if the objects compared are equals.
	 */
	public boolean equals(Position pos){
		if(this.ch==pos.ch && this.num==pos.num)
			return true;
		else
			return false;
	}
	
	/**
	 * Convert this object to its representation as String in the form of character and number, example "B2".
	 * @return the textual coordiates equivalent to this object.
	 */
	public String toString() {
		return new String(ch + "" + num);
	}
}

