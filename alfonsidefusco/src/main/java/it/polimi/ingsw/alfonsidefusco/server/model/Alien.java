package it.polimi.ingsw.alfonsidefusco.server.model;

import it.polimi.ingsw.alfonsidefusco.server.model.Cards.Card;

import java.rmi.RemoteException;
import java.util.List;

public class Alien extends Player {

	private int objectCardsNumber = 0;

	@Override
	public List<Card> getObjects(){
		return (List<Card>) object;
	}
	
	
	@Override
	public void addObject(Card card) {
		if(objectCardsNumber <= OBJECT_CARDS) {
			try {
				client.logDrawn();
			} catch (RemoteException e) {
				e.printStackTrace();
			}
			objectCardsNumber++;
		}
	}
	
	@Override
	public void removeObject(int cardNumber) {
		if(objectCardsNumber > 0) {
			try {
				client.logDiscard();
			} catch (RemoteException e) {
				e.printStackTrace();
			}
			objectCardsNumber--;
		}
	}
	
	@Override
	public void removeObject(Card card) {
		if(objectCardsNumber > 0) {
			try {
				client.logDiscard();
			} catch (RemoteException e) {
				e.printStackTrace();
			}
			objectCardsNumber--;
		}
	}



	public Alien(String name) {
	
		setCharacter(name);
	}
}

