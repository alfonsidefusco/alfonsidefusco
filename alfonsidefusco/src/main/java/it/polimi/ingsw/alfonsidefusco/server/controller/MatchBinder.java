package it.polimi.ingsw.alfonsidefusco.server.controller;

import it.polimi.ingsw.alfonsidefusco.Common.MatchInterface;
import it.polimi.ingsw.alfonsidefusco.Common.PlayerClientInterface;
import it.polimi.ingsw.alfonsidefusco.Common.PostmanInterface;
import it.polimi.ingsw.alfonsidefusco.server.model.Player;

import java.rmi.RemoteException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class MatchBinder implements Runnable {

	private static MatchInterface obj;
	private static PlayerClientInterface client;
	private PostmanInterface postman;
	private static MatchBinder instance;
	Executor ex = Executors.newCachedThreadPool();

	

	
	public void run() {
	
		String rules = null;
		String playground = null;
		try {
			rules = obj.getRules();
			playground = obj.getMap();
		} catch(RemoteException e) {
			e.printStackTrace();
		}
		MatchHandler handler = MatchGenerator.getInstance().findGame(rules, playground);
		Player player = handler.match.getCharacterCards().draw();
		player.setPostman(postman);
		player.setClient(client);
		handler.match.addMatchClient(obj);
		handler.match.addPlayer(player);
		try {
			player.getPostman().send("Welcome to this match. You play the role of " + player.getCharacter() + " as a " + player.getNature() );
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		
		if(!handler.alreadyRunning())
			ex.execute(handler);	
	}
	
	
	static MatchBinder getInstance() {
		if(instance == null )
			instance = new MatchBinder();
		return instance;
	}
	
	void toBind(MatchInterface match,  PlayerClientInterface name, PostmanInterface postman) {
		obj = match;
		client = name;
		this.postman = postman;
	}
	
	MatchBinder() {
	}
}
