package it.polimi.ingsw.alfonsidefusco.server.model.Cards.CharacterCards;

class Character {
	
	String name;
	String nature;
	
	Character(String name, String nature) {
		this.name = name;
		this.nature = nature;
	}
}
