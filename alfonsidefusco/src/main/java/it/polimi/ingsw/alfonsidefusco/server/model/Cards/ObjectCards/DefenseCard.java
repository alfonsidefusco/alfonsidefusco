package it.polimi.ingsw.alfonsidefusco.server.model.Cards.ObjectCards;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import it.polimi.ingsw.alfonsidefusco.server.model.Match;
import it.polimi.ingsw.alfonsidefusco.server.model.Actions.Defense;
import it.polimi.ingsw.alfonsidefusco.server.model.Player;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.Card;

public class DefenseCard implements Card {
	private static int quantity = 1;
	public static final String name = new String("Defense"); 
	
	@Override
	public boolean areFinished() {
		boolean finished = false;
		if (quantity == 0 )
			finished = true;
		return finished;	
	}
	
	@Override
	public void effect(Match match, Player player) throws Exception {
		Defense def = new Defense();
		Executor ex = Executors.newCachedThreadPool();
		def.action(match, player);
		ex.execute(def);
		ex = null;
		def = null;
	}
	
	@Override
	public void deduct() {
		quantity--;
	}

	public String toString() {
		return name;
	}
}
