package it.polimi.ingsw.alfonsidefusco.client.controller.cards;

public class Attack extends Card implements CardInterface {
	
	@Override
	public void interact() {
		screen.showMsg("You 'll attack in your current position");
	}
}
