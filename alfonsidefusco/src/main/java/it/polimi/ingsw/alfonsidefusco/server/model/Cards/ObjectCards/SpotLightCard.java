package it.polimi.ingsw.alfonsidefusco.server.model.Cards.ObjectCards;

import it.polimi.ingsw.alfonsidefusco.server.model.Match;
import it.polimi.ingsw.alfonsidefusco.server.model.Position;
import it.polimi.ingsw.alfonsidefusco.server.model.Actions.SpotLight;
import it.polimi.ingsw.alfonsidefusco.server.model.Player;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.Card;

public class SpotLightCard implements Card {
	private static int quantity = 2;
	public static final String name = new String("Spotlight"); 
	private Position target;
	
	@Override
	public boolean areFinished() {
		boolean finished = false;
		if (quantity == 0 )
			finished = true;
		return finished;	
	}
	
	@Override
	public void effect(Match match, Player player) throws Exception {
		SpotLight spot = new SpotLight();
		spot.setPos(target);
		spot.action(match, player);
	}
	
	@Override
	public void deduct() {
		quantity--;
	}

	public void setTargetPosition(Position pos) {
		this.target = pos;
	}

	public String toString() {
		return name;
	}
}
