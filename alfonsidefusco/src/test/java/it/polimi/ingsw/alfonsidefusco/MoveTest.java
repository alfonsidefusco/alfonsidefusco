package it.polimi.ingsw.alfonsidefusco;

import it.polimi.ingsw.alfonsidefusco.server.Exceptions.EscapeHatchClosedException;
import it.polimi.ingsw.alfonsidefusco.server.Exceptions.OutOfBoundsException;
import it.polimi.ingsw.alfonsidefusco.server.Exceptions.SectorNotFoundException;
import it.polimi.ingsw.alfonsidefusco.server.Exceptions.StupidUserException;
import it.polimi.ingsw.alfonsidefusco.server.controller.BasicMode;
import it.polimi.ingsw.alfonsidefusco.server.model.Human;
import it.polimi.ingsw.alfonsidefusco.server.model.Map;
import it.polimi.ingsw.alfonsidefusco.server.model.Match;
import it.polimi.ingsw.alfonsidefusco.server.model.Player;
import it.polimi.ingsw.alfonsidefusco.server.model.Position;
import it.polimi.ingsw.alfonsidefusco.server.model.Actions.Move;

import java.lang.Math;
import java.util.ArrayList;
import java.util.List;






import org.junit.runner.RunWith;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

public class MoveTest{
	Match partita =new Match("galilei");
	Player player1;
	Map mappa;
	int movement;
	Position removed= new Position("v02");
	
	@Before	
	public void setUp(){
	
	player1=new Human("tizio");
	mappa= new Map("galilei");
	
	
	partita.removeEscapeHatches(removed);
	
	}
	
	

	
	@Test(expected= EscapeHatchClosedException.class)
	public void EscapeHatchTest(){
		Match partita =new Match("galilei");
		Move a= new Move();
		Position esc= new Position("u03");
		player1=new Human("tizio");
		mappa= new Map("galilei");
		try {
			player1.setPosition(mappa.findSector(esc));
		} catch (SectorNotFoundException e) {
			e.printStackTrace();
		}catch(NullPointerException e){
			
		}
		
		a.setTargetPosition(removed);
		player1.setMobility(1);
		try {
			a.action(partita, player1);
		} catch (SectorNotFoundException e) {
			
		} catch (OutOfBoundsException e) {
			
		} catch (StupidUserException e) {
			
		} catch (EscapeHatchClosedException e) {
			
		}
	}
		@Test(expected= OutOfBoundsException.class)
		public void MoveLengthTest(){
			Match partita =new Match("galilei");
			Move a= new Move();
			Position start= new Position("m06");
			player1=new Human("tizio");
			mappa= new Map("galilei");
			try {
				player1.setPosition(mappa.findSector(start));
			} catch (SectorNotFoundException e) {
				e.printStackTrace();
			}
			catch(NullPointerException e){
				
			}
			Position dest= new Position("m08"); 
			a.setTargetPosition(dest);
			player1.setMobility(2);
			try {
				a.action(partita, player1);
			} catch (SectorNotFoundException e) {
				
			} catch (OutOfBoundsException e) {
				
			} catch (StupidUserException e) {
				
			} catch (EscapeHatchClosedException e) {
				
			}
	}
		
		@Test(expected= StupidUserException.class)
		public void WrongSectorsTest(){
			Match partita =new Match("galilei");
			Move a= new Move();
			Position start= new Position("m06");
			Position dest= new Position("l06"); 
			player1=new Human("tizio");
			mappa= new Map("galilei");
			a.setTargetPosition(dest);
			try {
				player1.setPosition(mappa.findSector(start));
			} catch (SectorNotFoundException e) {
				e.printStackTrace();
			}catch(NullPointerException e){
				
			}
			
			player1.setMobility(2);
			try {
				a.action(partita, player1);
			} catch (SectorNotFoundException e) {
				
			} catch (OutOfBoundsException e) {
				
			} catch (StupidUserException e) {
				
			} catch (EscapeHatchClosedException e) {
				
			}
		}
		
			public void RightSectorsTest(){
				Match partita =new Match("galilei");
				Move a= new Move();
				Position start= new Position("a04");
				Position dest= new Position("a06"); 
				player1=new Human("tizio");
				mappa= new Map("galilei");
				a.setTargetPosition(dest);
				try {
					player1.setPosition(mappa.findSector(start));
				} catch (SectorNotFoundException e) {
					e.printStackTrace();
				}catch(NullPointerException e){
					
				}
				
				player1.setMobility(2);
				try {
					a.action(partita, player1);
				} catch (SectorNotFoundException e) {
					
				} catch (OutOfBoundsException e) {
					
				} catch (StupidUserException e) {
					
				} catch (EscapeHatchClosedException e) {
					
				}
				
				assertEquals("the player didn't move",player1.getPosition(),dest);
	}
		
		
	}






