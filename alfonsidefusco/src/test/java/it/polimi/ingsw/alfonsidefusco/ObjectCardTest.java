package it.polimi.ingsw.alfonsidefusco;


import it.polimi.ingsw.alfonsidefusco.server.Exceptions.ReachObjectCardsLimitException;
import it.polimi.ingsw.alfonsidefusco.server.Exceptions.SectorNotFoundException;
import it.polimi.ingsw.alfonsidefusco.server.model.Alien;
import it.polimi.ingsw.alfonsidefusco.server.model.Human;
import it.polimi.ingsw.alfonsidefusco.server.model.Match;
import it.polimi.ingsw.alfonsidefusco.server.model.Player;
import it.polimi.ingsw.alfonsidefusco.server.model.Position;
import it.polimi.ingsw.alfonsidefusco.server.model.Sector;
import it.polimi.ingsw.alfonsidefusco.server.model.Cards.ObjectCards.*;

import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class ObjectCardTest {

	Match match = new Match("galilei");
	Player human = new Human("boh");
	
	
	private void setMatch() {
		match.addPlayer(human);
	}
	
	private Sector setPos(Player player) {
		Sector position = null;
		try {
			position = match.getMap().findSector( new Position("L5"));
		} catch (SectorNotFoundException e) {}
		try{
			player.setPosition(position);
		}
		catch(NullPointerException e) {
			
		}
		return position;
	}
	
	
	@Test
	public void AdrenalineTest() {
		setMatch();
		try {
			human.addObject( new AdrenalineCard() );
		}catch(NullPointerException | ReachObjectCardsLimitException e) {
		}
		try {
			human.getObjects().get(0).effect(match, human);
		} catch (Exception e) {
		}
		assertEquals("Mobility should be equal to 2", 2, human.getMobility());
	}
	
	@Test
	public void SedativeTest() {
		setMatch();
		try {
			human.addObject( new SedativeCard() );
		}catch(NullPointerException | ReachObjectCardsLimitException e) {
		}
		try {
			human.getObjects().get(0).effect(match, human);
		} catch (Exception e) {
		}
		assertTrue("Player has used a sedative card", ((Human)human).hasSedative());
	}

	@Test
	public void AttackTest() {
		setMatch();
		Player alien = new Alien("alien");
		match.addPlayer(alien);
		setPos(human);
		setPos(alien);
		try {
			human.addObject( new AttackCard() );
		} catch ( NullPointerException e ) {
		}
		catch( ReachObjectCardsLimitException e) {}
		
		try {
			human.getObjects().get(0).effect(match, human);
		} catch (Exception e) {	}
		assertTrue("Rival players in same position should be dead", alien.isDead());
		
	}
	
	@Test
	public void DefenseCard() {
		setMatch();
		try {
			human.addObject( new DefenseCard() );
		} catch ( NullPointerException | ReachObjectCardsLimitException e ) {
		}
		try {
			human.getObjects().get(0).effect(match, human);
		} catch (Exception e) {	}
		assertTrue("Player should be set to invincible", human.isInvincible());
	}
	
	@Test
	public void SpotLightCard() {
		/*setMatch();
		Player alien = new Alien("alien");
		match.addPlayer(alien);
		PlayerClient playerClient = new PlayerClient();
		PlayerClientInterface playercliInterf = (PlayerClientInterface) playerClient;
		alien.setClient(playercliInterf);
		
		//position to be light
		Sector sec = setPos(alien);
	

		
		
		try {
			human.addObject( new SpotLightCard() );
		} catch ( NullPointerException | ReachObjectCardsLimitException e ) {
		}
		try {
			
			SpotLightCard spot = (SpotLightCard) human.getObjects().get(0);
			spot.setTargetPosition(sec);
			spot.effect(match, human);
		} catch (Exception e) {	}
		Position publicPos = null;
		publicPos = new Position( playerClient.getPublicPosition() );
		assertEquals("Public Position of the player should be equal o private one", true, publicPos.equals(alien.getPosition()));
	*/
	}
	
	@Test
	public void TeleportCard() {
		setMatch();
		try {
			match.setStartPosition();
		}
		catch (NullPointerException e) {}
		
		//catch position of starting sector
		Sector startPosition = human.getPosition();
		
		//move player around
		setPos(human);
		try {
			human.addObject( new TeleportCard() );
		} catch ( NullPointerException | ReachObjectCardsLimitException e ) {
		}
		try {
			human.getObjects().get(0).effect(match, human);
		} catch (Exception e) {	}
		assertEquals("IStarting position should be equals to the current one", true,  startPosition.equals(human.getPosition()));
		
	}
	
}
